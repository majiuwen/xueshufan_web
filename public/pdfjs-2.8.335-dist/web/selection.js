(function () {
  window.SelectionRender = function () {
    this.colors = [
      "#FFD400",
      "#FF0000",
      "#26FC00",
      "#FF6F00",
      "#00A6FF",
    ];

    this.hlDefaultColor = "rgba(0, 0, 255, 1)";
    this.lastHLDefaultColor = window.localStorage.getItem('lastHLDefaultColor') || this.colors[0]

    this.toolbar = [
      { text: "摘录图片", type: "cut" },
      { text: "删除高亮", type: "delHighLight" },
    ];

    this.selectionPageNumber = 1; //当前点击所在页数
    this.pageEle = undefined;
    this.hasMove = false; // 鼠标移动或点击
    this.range = null; // 鼠标移动或点击
    this.onClick = false; // 鼠标是否点击（用于mousemove区分是否在点击时移动）

    this.clickActionType = ""; // 当前点击的功能
    this.options = {
      hideFunc: false, // 是否隐藏所有工具  默认不隐藏
      isLeftEdit: false, // 是否是边写边搜  默认不是边写边搜
    };
    this.rectOptions = {};
    this.cutTextObj = null;

    this.hasOnAnnotation = false; // 是否点击到自带批注
    this.hasRectSelected = false; // 是否是框选

    // pdfjs 对象
    this.PDFViewerApplication = null;
    this.pdfViewer = null;

    this.toolbarIsShow = false;
    this.colorBarIsShow = false;
    this.tempSelection = {
      highlight_info: {},
    };
    this.selectionText = null;
    this.highLightData = [];
    this.lastPage = 1;
    this.isDownSelect = false;
    this.drawImage = null;
    this.lhqBtn = null; // 快捷按钮
    this.lhqColorEle = null; // 快捷高亮 颜色容器
    this.quickColor = null;

    this.wordsNinja = null;
    this.ignoreNextMouseDown = false;

    this.clipboard = null;
    this.isPC = !(/Android|webOS|iPhone|iPod|BlackBerry|iPad/i.test(navigator.userAgent));
    this.hammertime = null;
    // 添加事件
    this.addEvent = function () {
      // 鼠标点击监听
      const viewerContainer = document.getElementById('viewerContainer');

      if(this.isPC) {
        viewerContainer.addEventListener("mousedown", this.mouseDown.bind(this), true);
        viewerContainer.addEventListener("mousemove", this.mouseMove.bind(this), true);
        viewerContainer.addEventListener("mouseup", this.mouseUp.bind(this), true);
      } else {
        viewerContainer.addEventListener("touchstart", function(e) {
          const pageEle = $($(e.target)).closest(".page")[0];
          if (pageEle) {
            this.selectionPageNumber = $(pageEle).data("pageNumber");
            this.pageEle = pageEle;
          }

          if (
            !this.toolbarIsShow ||
            (this.toolbarIsShow && (
                $(e.target).parents("#fir-tool-bar").length === 0 &&
                !window.getSelection().toString()
              )
            )
          ) {

            this.removeHLMaskEle("temp");
            this.hideToolbar();
          }
        }.bind(this), true);

        viewerContainer.addEventListener("touchend", this.touchend.bind(this), true);

        viewerContainer.addEventListener('contextmenu', e => {
          e.preventDefault();
        });
      }

      $("#viewer").on("mousemove", function (e) {
        if(/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent)) {
          return
        }
        if (
          $(e.target).parents(".textLayer").length === 0 ||
          $(e.target).hasClass("hl-mask-selection")
        ) {
          $("#viewer").addClass("can-not-select");
        } else {
          $("#viewer").removeClass("can-not-select");
        }
      });

      $("#preScreen2, #preScreen").on("click", () => {
        let pv = $('div[data-page-number="' + this.lastPage + '"]');
        if (pv && pv.length > 0) {
          if (pv.length == 2) pv[1].scrollIntoView(false);
          else pv[0].scrollIntoView(false);
        }
      });

      this.lhqColorEle = $('.lhq-color-box');
      this.lhqBtn = $('.highlight-btn');
      var deley = 300, click=0, timer=null;
      this.lhqBtn.on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        const _this = this;
        //每次点击，点击次数加1
        click++;
        //判断是否单击事件
        if(click == 1){
          //设置定时器，当超过一定时间没有在点击，认为时单击事件
          timer = setTimeout( function(){
            click=0;
            _this.lhqBtnClick(e);
          }, deley)
        } else {
          clearTimeout(timer);
          click=0;
          _this.lhqBtnDBClick(e);
        }
      }.bind(this));
      $('.lhq-clear').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        this.lhqBtnDBClick()
      }.bind(this))

      this.onPinch()
    };

    this.mouseDown = function (e) {
      const pageEle = $($(e.target)).closest(".page")[0];
      if (pageEle) {
        this.selectionPageNumber = $(pageEle).data("pageNumber");
        this.pageEle = pageEle;
      }
      this.onClick = true;
      this.hasMove = false;
      this.hasOnAnnotation = false;
      this.hasRectSelected = false;
      this.move = false;

      if ($(e.target).parents("#fir-tool-bar").length === 0) {
        this.initRectDiv();
      }

      if (this.isOnAnnotation(e)) {
        this.onAnnotation(e);
      }
      // 是否点击在矩形区域内
      clickOnRect(e);
      //
      this.removeHLMaskElePre();
      if (
        !this.options.hideFunc &&
        !this.onLayerSpan(e) &&
        !this.onSelection(e) &&
        $(e.target).parents("#fir-tool-bar").length === 0 &&
        this.onTextLayer(e) &&
        !this.quickColor
      ) {
        // 框选
        this.hasRectSelected = true;
        this.removeHLMaskEle("temp");
        $(".new-selection-warp").remove();
        this.createCroordsDiv(e); // 初始化div
      }
      if (
        !this.toolbarIsShow ||
        (this.toolbarIsShow &&
          $(e.target).parents("#fir-tool-bar").length === 0)
      ) {
        if (this.hasOnAnnotation) {
          return;
        }
        this.removeHLMaskEle("temp");
        this.hideToolbar();
      }
    }

    this.mouseMove = function (e) {
      if(!(/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent))) {
        if (
          $(e.target).parents(".textLayer").length === 0 ||
          $(e.target).hasClass("hl-mask-selection") ||
          $(e.target).hasClass("textLayer")
        ) {
          $("#viewer").addClass("can-not-select");
        } else {
          $("#viewer").removeClass("can-not-select");
        }
      }

      this.hasMove = true;
      this.onMouseMove(e);
    }

    // 选择整个单词
    this.getSelectionWord = function () {
      let sel;
      if (window.getSelection && (sel = window.getSelection()).modify) {
        sel = window.getSelection();
        if (!sel.isCollapsed) {

          // Detect if selection is backwards
          let range = document.createRange();
          range.setStart(sel.anchorNode, sel.anchorOffset);
          range.setEnd(sel.focusNode, sel.focusOffset);
          let backwards = range.collapsed;
          range.detach();

          // modify() works on the focus of the selection
          let endNode = sel.focusNode, endOffset = sel.focusOffset;
          sel.collapse(sel.anchorNode, sel.anchorOffset);

          let direction = [];
          if (backwards) {
            direction = ['backward', 'forward'];
          } else {
            direction = ['forward', 'backward'];
          }

          sel.modify("move", direction[0], "character");
          sel.modify("move", direction[1], "word");
          sel.extend(endNode, endOffset);
          sel.modify("extend", direction[1], "character");
          sel.modify("extend", direction[0], "word");
        }
      } else if ((sel = document.selection) && sel.type != "Control") {
        let textRange = sel.createRange();
        if (textRange.text) {
          textRange.expand("word");
          while (/\s$/.test(textRange.text)) {
            textRange.moveEnd("character", -1);
          }
          textRange.select();
        }
      }
    }
    this.mouseUp = function (e) {

      const selectionStr =  window.getSelection().toString();
      this.getSelectionWord();
      if (selectionStr.length > 1000) {
        window.parent.getErrorMessage('批注内容1000字以内');
        return;
      }
      if(!this.hasMove) {
        $('.drawWrapper').remove()
      }
      if (this.hasRectSelected) {
        this.onClick = false;
        this.hasMove = false;
        return;
      }
      this.hasRectSelected = false;
      if (this.hasMove) {
        this.onMouseMove(e);
      } else {
        if(selectionStr) {
          this.onMouseMove(e);
        } else if (
          !this.toolbarIsShow ||
          (this.toolbarIsShow &&
            $(e.target).parents("#fir-tool-bar").length === 0)
        ) {
          if (this.hasOnAnnotation) {
            return;
          }
          this.removeHLMaskEle("temp");
          this.hideToolbar();
        }
      }
      this.onClick = false;
      this.hasMove = false;
    }

    this.touchend = function(e) {

      this.onMouseMove(e);
      throw new Error("NO ERRPR:禁止长按弹出的菜单");
    }

    this.onPinch = function() {
      const element = document.getElementById('viewer');
      delete Hammer.defaults.cssProps.userSelect;
      // Hammer.defaults.domEvents = true;
      this.hammertime = new Hammer(document.getElementById('viewer'), { touchAction: 'pan-x pan-y' });
      this.hammertime.get('pinch').set({ enable: true });
      this.hammertime.on('pinch', function(e) {
        const {additionalEvent, scale} = e;
        if(additionalEvent == 'pinchin' || additionalEvent == 'pinchout') {
          this.pdfViewer.currentScale = scale;
        }
      }.bind(this));
    }

    this.lhqBtnClick = (e) => {
      const display = this.lhqColorEle.css('display');
      if(e.target.nodeName == 'LI') {
        this.selectQuickHL(e.target)
      } else {
        if(display === 'none' && !this.quickColor) {
          // 第一次
          const liEle = $('.lhq-color-box li:first');
          this.selectQuickHL(liEle[0])
        }
      }
      this.lhqColorEle.css({display: display === 'none' ? 'block' : 'none'})
    }
    this.selectQuickHL = ele => {
      this.quickColor = ele.dataset.color;
      $('.lhq-color-box li').removeClass('seleted-color')
      $(ele).addClass('seleted-color');
      this.lhqBtn.addClass('cursor-selected');
      $('#qsColor').attr({fill: this.quickColor});
      if(!this.quickColor) {
        this.cancelQHL()
      } else {
        $('.lhq-clear').css({display: 'inline-block'});
        // this.lhqColorEle.css({left: -36})
      }
    }
    this.lhqBtnDBClick = (e) => {
      this.quickColor = null;
      this.lhqColorEle.css({display: 'none'})
      this.cancelQHL();
    }
    this.cancelQHL = () => {
      this.lhqBtn.removeClass('cursor-selected')
      $('.lhq-color-box li').removeClass('seleted-color')
      $('#qsColor').attr({fill: '#D7D7DB'});
    }

    // 创建工具栏容器
    this.createToolbarWarp = function () {
      if ($("#fir-tool-bar").length > 0) {
        $("#fir-tool-bar").remove();
      }
      const warp = $(
        '<div id="fir-tool-bar">\n' +
        '      <div class="fir-tool-box">\n' +
        '        <div class="fir-tool-item toolbar" style="display: none"><a></a></div>\n' +
        "      </div>\n" +
        "    </div>"
      );

      $("#printContainer").after(warp);
    };
    // 初始化工具栏
    this.initToolbar = function () {
      this.createToolbarWarp();
      // 创建toolbar
      const toolbarEles = [];
      if (this.options.isLeftEdit) {
        this.toolbar = this.toolbar.filter(
          (item) =>
            [
              "excerpt_text",
              "delHighLight",
              "cut",
            ].indexOf(item.type) === -1
        );
      } else {
        this.toolbar = this.toolbar.filter(
          (item) => ["import_text"].indexOf(item.type) === -1
        );
      }
      this.toolbar.forEach((v) => {
        const ele = $(
          '<div class="fir-tool-item" data-type="' +
          v.type +
          '"><a>' +
          v.text +
          "</a></div>"
        );
        if(v.type === 'copy') {
          ele.addClass('copy-btn')
        }
        toolbarEles.push(ele);
      });
      const firToolBoxLastItem = $(".fir-tool-item.toolbar").first();
      firToolBoxLastItem.before(toolbarEles);

      // 绑定事件
      $(".fir-tool-box").on("click", "div", this.onToolClick.bind(this));
      $("#fir-tool-bar [data-type='delHighLight']").bind("click", this.removeHighLight.bind(this));
    };

    // 点击到自带批注处理
    this.onAnnotation = async function (e) {
      this.createSelectParamsOnAnnotation(e);
      $(".hl-color-box").hide();
      this.colorBarIsShow = false;
      this.toolbarIsShow = false;
      this.hasOnAnnotation = true;
      this.showToolbar(e);
    };
    this.renderRect = async function(position) {
      if(position.width > 0 && position.height > 0) {
        await this.buildRectHighlightInfo(position)
        this.showToolbar(position, undefined, 'rect')
      }
    }
    this.cancelRect = function() {
      this.hasRectSelected = false;
      this.hideToolbar()
    }

    //
    this.createCroordsDiv = function (e) {
      this.drawImage.init(this.selectionPageNumber, e, this.renderRect.bind(this), this.cancelRect.bind(this))
    };

    this.moveCroordsDiv = function (e) {
      this.move = true;
    };

    this.initRectDiv = function (close) {
      this.rectOptions = {};
      $("#currentRectDiv").remove();
      this.hideToolbar();

      if(close) {
        this.hasRectSelected = false;
        this.drawImage.cancelRectRange()
      }
    };

    // 获取页面宽度和高度，并且进行top和left转换
    this.calculatePageRage = function (top, left) {
      let width = getClearWidth($(this.pageEle).css("width"));
      let height = getClearWidth($(this.pageEle).css("height"));
      if (top && top > 0) top = (top / height).toFixed(4) * 100;
      if (left && left > 0) left = (left / width).toFixed(4) * 100;
      return { width: width, height: height, top: top, left: left };
    };
    // 判断是否点击到自带批注
    this.isOnAnnotation = function (event) {
      if (
        event &&
        event.target &&
        event.target.nodeName == "SECTION" &&
        event.target.className.indexOf("Annotation") !== -1
      ) {
        return true;
      }
      return false;
    };
    //
    this.createSelectParamsOnAnnotation = function (event) {
      let annotationId = $(event.target).attr("data-annotation-id");
      const sections = $(`section[data-annotation-id="${annotationId}"]`);
      let rectTop = 9999;
      let rectLeft = 9999;
      let rectBottom = 0;
      let rectRight = 0;
      for (let i = 0; i < sections.length; i++) {
        const section = sections[i];
        const targetStyle = section.style;
        let transforms = targetStyle.transform
          .replace("matrix(", "")
          .replace(")", "")
          .split(",");

        let top = this.getClearWidth(targetStyle.top) * transforms[0];
        let left = this.getClearWidth(targetStyle.left) * transforms[0];
        let height = this.getClearWidth(targetStyle.height);
        let width = this.getClearWidth(targetStyle.width);
        let bottom = top + height;
        let right = left + width;
        if (top < rectTop) rectTop = top;
        if (left < rectLeft) rectLeft = left;
        if (bottom > rectBottom) rectBottom = bottom;
        if (right > rectRight) rectRight = right;
      }

      let rectHeight = rectBottom - rectTop;
      let rectWidth = rectRight - rectLeft;

      let position = {
        rectTop: rectTop,
        rectLeft: rectLeft,
        rectBottom: rectBottom,
        rectRight: rectRight,
      };
      let rect = {
        top: (
          (rectTop / this.getClearWidth($(this.pageEle).css("height"))) *
          100
        ).toFixed(4),
        left: (
          (rectLeft / this.getClearWidth($(this.pageEle).css("width"))) *
          100
        ).toFixed(4),
        height: (
          rectHeight / this.getClearWidth($(this.pageEle).css("height"))
        ).toFixed(4),
        width: (
          rectWidth / this.getClearWidth($(this.pageEle).css("width"))
        ).toFixed(4),
      };
      let obj = this.getSpansAndNumbers(position);
      let selectText = "";
      if(Array.isArray(obj.spans)) {

        $.each(obj.spans, function(i, ele){
          selectText += ele.innerText;
        })
      }
      this.selectionText = selectText;

      this.buildTempSelection({
        range: {
          start: obj.numbers[0],
          end: obj.numbers[obj.numbers.length - 1],
          startOffset: 0,
          endOffset: obj.spans[obj.spans.length - 1].innerText.length,
        },
        rect: rect,
        selectContent: selectText,
        annotation: 1,
        annotationId: annotationId,
      });


    };
    this.getSpansAndNumbers = function (position) {
      const childNodes = $(
        'div[data-page-number="' +
        this.selectionPageNumber +
        '"] div.textLayer span'
      );
      const notEmpty = [];
      childNodes.each((i, ele) => {
        if (!!$.trim($(ele).text())) {
          notEmpty.push(ele);
        }
      });
      let spans = [];
      let numbers = [];
      for (let i = 0; i < notEmpty.length; i++) {
        let node = notEmpty[i];
        let spanTop = this.getClearWidth(node.style.top);
        let spanLeft = this.getClearWidth(node.style.left);
        let spanHeight = this.getClearWidth($(node).css("height"));
        let spanWidth = this.getClearWidth($(node).css("width"));
        let spanBottom = spanTop + spanHeight;
        let spanRight = spanLeft + spanWidth;

        if (
          position.rectTop < spanBottom &&
          position.rectBottom > spanTop &&
          !(spanRight < position.rectLeft || spanLeft > position.rectRight)
        ) {
          spans.push(node);
          numbers.push(i);
        }
      }
      return { spans: spans, numbers: numbers };
    };
    this.buildRectHighlightInfo = async function (rectPosition) {
      let pageParam = this.calculatePageRage();
      let div = $("#currentRectDiv");
      let rect = {}
      if(rectPosition) {
        rect = {
          top: rectPosition.y,
          left: rectPosition.x,
          height: rectPosition.height,
          width: rectPosition.width,
        };
      } else {
        rect = {
          top: this.getClearWidth(div.css("top")),
          left: this.getClearWidth(div.css("left")),
          height: this.getClearWidth(div.css("height")),
          width: this.getClearWidth(div.css("width")),
        };
      }

      let position = {
        rectTop: rect.top,
        rectLeft: rect.left,
        rectBottom: rect.top + rect.height,
        rectRight: rect.left + rect.width,
      };
      let obj = this.getSpansAndNumbers(position);

      let highlight_info = {
        uid: getUUID(),
        selectContent: this.selectionText,
        rect: {
          top: (parseFloat(rect.top) / parseFloat(pageParam.height)) * 100,
          left: (parseFloat(rect.left) / parseFloat(pageParam.width)) * 100,
          height: parseFloat(rect.height) / parseFloat(pageParam.height),
          width: parseFloat(rect.width) / parseFloat(pageParam.width),
        },
        range: {
          start: obj.numbers[0],
          end: obj.numbers[obj.numbers.length - 1] + 1,
          startOffset: 0,
          endOffset: 0,
        },
        page_num: this.selectionPageNumber,
        rectMode: true,
        preRender: true,
      };
      this.tempSelection.highlight_info = highlight_info;
    };
    this.buildTempSelection = function (extend = {}) {
      this.tempSelection = {
        highlight_info: {
          page_num: this.selectionPageNumber,
          ...extend,
        },
      };
    };
    this.canvasDrawImage = function (rect) {
      let page_num = this.selectionPageNumber;
      let canvas = $(
        "div[data-page-number='" +
        this.selectionPageNumber +
        "'] .canvasWrapper canvas"
      )[0];
      if (canvas) {
        let pageWidth = getClearWidth(canvas.getAttribute("width"));
        let pageHeight = getClearWidth(canvas.getAttribute("height"));
        let tempSelection = this.tempSelection;
        let rect = this.tempSelection.highlight_info.rect;
        let x = rect.left * 0.01 * pageWidth;
        let y = rect.top * 0.01 * pageHeight;
        let swidth = rect.width * pageWidth;
        let sheight = rect.height * pageHeight;
        let newCanvas = document.createElement("canvas");
        newCanvas.width = swidth;
        newCanvas.height = sheight;
        newCanvas.style.position = "absolute";
        newCanvas.style.top = y - 50 + "px";
        newCanvas.style.left = x - 50 + "px";
        newCanvas.style.zIndex = "9999";
        let ctx = newCanvas.getContext("2d");
        ctx.drawImage(canvas, x, y, swidth, sheight, 0, 0, swidth, sheight);
        // $(currentPageDiv).append(newCanvas)
        newCanvas.toBlob(function (blob) {
          let message = {
            content: "",
            actionType: "cut",
            params: {
              actionType: "cut",
              blob: blob,
              uid: getUUID(),
              page_num: page_num,
              rect: rect,
              swidth: swidth,
              sheight: sheight
            },
            tempSelection: tempSelection
          };
          window.parent.getImage(message);
        });
      }
    };
    // 点击工具栏
    this.onToolClick = function (e) {

      const type = $(e.currentTarget).data("type");
      if(type != 'copy') {
        e.stopPropagation();
        e.preventDefault();
      }
      this.clickActionType = type;
      if (type === "copy") {
        // this.onCopyText();
      }
      if (type === "excerpt_text" || type === "import_text") {
        this.onExcerpt();
      }
      if (type === "cut") {
        this.canvasDrawImage(this.rectOptions.rect);
        this.initRectDiv(true);
      }
      if (type === "delHighLight") {
        this.onDelHighLight(this);
      }
    };
    // 提取
    this.onExcerpt = function (e) {
      if (!this.tempSelection.highlight_info.color) {
        this.tempSelection.highlight_info.uid = this.getUUID();
      }
      this.onMessage('excerpt_text',"","",e);
    };
    // 翻译
    this.onDelHighLight = function () {
      this.hideToolbar();
      this.initRectDiv(true);
    };
    // 切换颜色
    this.selectColor = function (e) {
      const isAuto = typeof e === "string";
      const color = isAuto ? e : $(e.currentTarget).data("color");
      this.lastHLDefaultColor = color;
      window.localStorage.setItem('lastHLDefaultColor', color);
      this.addHighlightData(color);

      $(".color-warp span").removeClass("selected");
      $('.color-warp span[data-color = "' + color + '"]').addClass("selected");
      if (!isAuto) {
        $(".hl-color-box").css("display", "none");
        this.hideToolbar();
        this.colorBarIsShow = false;
      } else {
        this.bindMaskClick(undefined, this.clickActionType);
      }
    };
    // 高亮
    this.onHighlight = function () {
      this.colorBarIsShow = !this.colorBarIsShow;
      const display = this.colorBarIsShow ? "flex" : "none";
      if (this.colorBarIsShow) {
        const color = "#FFD400";
        // this.tempSelection.highlight_info?.color || this.lastHLDefaultColor;
        this.selectColor(color);
      }
      $(".hl-color-box").css("display", display);
    };
    // 点击高亮遮罩
    this.bindMaskClick = function (info, actionType) {
        const uid = (info && info.uid) || this.tempSelection.highlight_info.uid;
        const selector = "#" + uid;
        $(selector).on(
          "click",
          function (e) {
            const index = this.highLightData.findIndex(v => v.highlight_info.uid == uid);
            this.tempSelection = this.highLightData[index]
            this.selectionText =
              (info && info.selectContent) ||
              this.tempSelection.highlight_info.selectContent;

            this.createSelectTag(uid)
            this.showToolbar(e, uid);
          }.bind(this)
        );
    };

    this.createSelectTag = function(uid) {
      $('.select-tag-warp').remove()

      const selector = `.page[data-page-number=${this.selectionPageNumber}] .textLayer`;
      const parentEle = $(selector);
      const eleSelector = `#${uid} [data-uid=${uid}]`
      const eles = $(eleSelector);
      $(`#${uid} img.sign`).remove();
      const styles = [];
      eles.each((i, ele) => {
        const obj = $(ele).css(['left', 'top', 'width', 'height']);
        for (const key in obj) {
          if (Object.hasOwnProperty.call(obj, key)) {
            obj[key] = parseFloat(obj[key])
          }
        }
        styles.push(obj)
      })
      let warpSize = {
        minTop: null,
        maxTop: 0,
        minLeft: null,
        maxLeft: 0,
        lastHeight: null
      }

      warpSize = styles.reduce((res, curVal, index) => {
        const {top, left, width, height} = curVal
        if(index === 0) {
          res.minTop = top;
          res.minLeft = left;
        }

        res.minTop = res.minTop > top ? top : res.minTop;
        res.maxTop = res.maxTop > top ? res.maxTop : top;
        res.minLeft = res.minLeft > left ? left : res.minLeft;
        res.maxLeft = res.maxLeft > (left + width) ? res.maxLeft : (left + width) ;
        if(warpSize.maxTop == top) {
          warpSize.lastHeight = height;
        }
        return res;
      }, warpSize)

      const warpStyles = {
        position: "absolute",
        left: warpSize.minLeft - 2.5,
        top: warpSize.minTop - 2.5,
        height: warpSize.maxTop - warpSize.minTop + warpSize.lastHeight + 2,
        width: warpSize.maxLeft - warpSize.minLeft + 2,
        border: '2px dashed #55C0FF',
        borderRadius: '2px',
        zIndex: 9,
      }
      const maskWarp = $(
        '<div class="select-tag-warp"></div>'
      );
      maskWarp.css(warpStyles)
      parentEle.append(maskWarp);
    }

    // 快捷键删除
    this.deleteMask = function() {
      let ele = $('.select-tag-warp');

      if(ele.length === 1) {
        const actionType = this.tempSelection.actionType;
        if(actionType === 'excerpt_text' || actionType === "delHighLight" || !actionType) {
          // 删除摘录
          window.parent.postMessage({
            actionType: "deleteNote",
            data: {...this.tempSelection}
          }, "*");
          if(!actionType){
            this.removeHighLight()
          }
        } else {
          // 删除高亮
          this.removeHighLight()
        }
      }
    }
    // 快捷高亮
    this.quickLighHight = function(e) {
      this.clickActionType = 'highlight';
      // this.selectColor(this.quickColor);
      this.onMessage(this.clickActionType,"","",e);
    }
    // 获取渲染样式
    this.getPreSaveSelectionCssObj = function (actionType, color) {
      if (actionType == "underline" || actionType == "changeUnderline") {
        return {
          borderBottom: "2px solid",
          borderColor: color,
        };
      }
      return {
        backgroundColor: color,
      };
    };

    this.addHighlightData = function (color) {
      this.tempSelection.highlight_info.color = color;
      const uid = this.tempSelection.highlight_info.uid;
      if (uid) {
        $(".save-selection[data-uid = " + uid + "]").css(
          this.getPreSaveSelectionCssObj(this.clickActionType, color)
        );

        const index = this.highLightData.findIndex(
          (v) => v.highlight_info.uid + "" === uid + ""
        );
        if (index > -1) {
          let obj = { ...this.tempSelection }
          this.highLightData.splice(index, 1, obj);
        }
      } else {
        this.tempSelection.highlight_info.uid = new Date().getTime() + "";
        this.tempSelection.actionType = this.clickActionType;
        this.highLightData.push(this.tempSelection);
        this.reRenderHighLight([this.tempSelection], false, true);
      }
    };
    // 擦除
    this.removeHighLight = function (isNotHide) {

      const uid = this.tempSelection.highlight_info.uid;
      const excerptId = this.tempSelection.highlight_info.excerptId;
      let message = {
        actionType: "highlight",
        subActionType: "clear",
        params: { uid },
        excerptId: excerptId
      };
      window.parent.delExcerpt(message);
      this.removeHLMaskEle();
      if (!isNotHide || typeof isNotHide === "object") {
        this.hideToolbar();
      }
      this.removeHighLightData(uid);
    };
    this.removeHighLightData = function(uid) {
      const index = this.highLightData.findIndex(v => (v.uid == uid || v.highlight_info.uid == uid))
      if(index > -1) {
        this.highLightData.splice(index, 1)
      }
    }
    // 显示工具栏
    this.showToolbar = function (event, uid, type) {
      let rectPos = {}
      let e = type === 'rect' ? window.event : event;
      if(type === 'rect') {
        rectPos = event
      }
      if (this.options.hideFunc) {
        this.toolbarIsShow = false;
        return;
      }
      this.toolbarIsShow = true;
      const selector = `.page[data-page-number=${this.selectionPageNumber}] .textLayer`;
      const saveSelector = "#" + uid + " div.save-selection";
      const parentEle = $(selector);
      const docWidth = parentEle.width();
      const clientRect = parentEle.parents()[0].getBoundingClientRect();
      const pageX = e.pageX;
      const pageY = e.pageY;
      const firToolBar = $("#fir-tool-bar");

      const toolbarPos = {
        display: "block",
        left: 0,
        top: pageY + 15,
      };


      const newSelections = uid ? $(saveSelector) : $(".new-selection");
      const lastSelection = $(newSelections[newSelections.length - 1]);
      const lastTop = parseInt(lastSelection.css("top"));
      const lastLeft = parseInt(lastSelection.css("left"));
      const lastWidth = parseInt(lastSelection.css("width"));

      const firstSelection = $(newSelections[0]);
      const firstTop = parseInt(firstSelection.css("top"));
      const firstLeft = parseInt(firstSelection.css("left"));

      toolbarPos.top = lastTop
        ? lastTop + firToolBar.height()
        : e.clientY - clientRect.top;

      // 判断是否已选择高亮颜色
      const selectedColor =
        this.tempSelection.highlight_info &&
        this.tempSelection.highlight_info.color;

      // 展示所有按钮
      firToolBar.find("div[data-type]").map((i, item) => {
        $(item).css('display', 'block');
      });

      if (selectedColor) {
        // 切换颜色  隐藏高亮和下划线按钮 隐藏
        const actionType = this.tempSelection.actionType || this.tempSelection.highlight_info.actionType;
        const isUnderline =
          actionType == "underline" || actionType == "changeUnderline";
        const hideDataType = isUnderline ? "changeLight" : "changeUnderline";
        firToolBar.find('div[data-type="' + hideDataType + '"]').hide();
      }

      if (uid && !selectedColor) {
        firToolBar.find('div[data-type="excerpt_text"]').hide();
      } else {
        firToolBar.find('div[data-type="excerpt_text"]').show();
      }
      if (type === 'rect') {
        firToolBar.find('div[data-type="cut"]').show();
        firToolBar.find('div[data-type="delHighLight"]').hide();
        firToolBar.find('div[data-type="highlight"]').hide();
        firToolBar.find('div[data-type="changeLight"]').hide();
        firToolBar.find('div[data-type="underline"]').hide();
        firToolBar.find('div[data-type="changeUnderline"]').hide();
        if (this.selectionText) {
          firToolBar.find('div[data-type="excerpt_text"]').show();
          firToolBar.find('div[data-type="copy"]').show();
        } else {
          firToolBar.find('div[data-type="excerpt_text"]').hide();
          firToolBar.find('div[data-type="import_text"]').hide();
          firToolBar.find('div[data-type="copy"]').hide();
          firToolBar.find('div[data-type="delHighLight"]').hide();
        }
      } else {
        firToolBar.find('div[data-type="cut"]').hide();
      }
      if (this.hasOnAnnotation) {
        firToolBar.find('div[data-type="highlight"]').hide();
        firToolBar.find('div[data-type="changeLight"]').hide();
        firToolBar.find('div[data-type="underline"]').hide();
        firToolBar.find('div[data-type="changeUnderline"]').hide();
      }
      let firTBWidth = $("#fir-tool-bar").width();

      if(this.isDownSelect) {
        toolbarPos.top = firstTop ? firstTop - firToolBar.height() - 10
          : e.clientY - clientRect.top;
        toolbarPos.left = (firstLeft - firTBWidth/2)
      } else {
        toolbarPos.left = lastLeft + lastWidth - firTBWidth/2
      }



      const pageSelector = `.page[data-page-number=${this.selectionPageNumber}]`;
      $(pageSelector).append(firToolBar);

      if(type === 'rect') {
        toolbarPos.top = rectPos.y + rectPos.height + 10
        toolbarPos.left = rectPos.x;
        toolbarPos.zIndex = 10000
      }
      firTBWidth = $("#fir-tool-bar").width();
      if(toolbarPos.left < 0) {
        toolbarPos.left = 0;
      }

      if(toolbarPos.left > (docWidth - firTBWidth)) {
        toolbarPos.left = docWidth - firTBWidth;
      }
      firToolBar.css(toolbarPos);

      const str = this.tempSelection.highlight_info.selectContent;
      const _this = this;
      this.clipboard = new ClipboardJS('.copy-btn', {
        text: function(trigger) {
          return str;
        }
      });

      this.clipboard.on('success', function(e) {
        _this.onCopyText()
      });

      this.clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
      });
    };

    this.ToCDB = function(str) {
      if(!str) return str;
      var tmp = "";
      for(var i=0;i<str.length;i++){
        if (str.charCodeAt(i) == 12288){
          tmp += String.fromCharCode(str.charCodeAt(i)-12256);
          continue;
        }
        if(str.charCodeAt(i) > 65280 && str.charCodeAt(i) < 65375){
          tmp += String.fromCharCode(str.charCodeAt(i)-65248);
        }
        else{
          tmp += String.fromCharCode(str.charCodeAt(i));
        }
      }
      return tmp
    }
    // 隐藏工具栏
    this.hideToolbar = function () {
      $(".hl-color-box").hide();
      $("#fir-tool-bar").hide();
      $('.select-tag-warp').remove()
      this.colorBarIsShow = false;
      this.toolbarIsShow = false;

      this.selectionText = null;
      this.clickActionType = "";
      // let().removeAllRanges()

      this.removeHLMaskEle("temp");

    };
    // 复制
    this.onCopyText = function () {

      this.hideToolbar();
      document.getElementsByClassName("fn-tips")[0].style.display = "block";
      setTimeout(function () {
        document.getElementsByClassName("fn-tips")[0].style.display = "none";
      }, 1500);
      this.initRectDiv(true);
    };

    this.onNewCopyText = function() {
      const str = this.tempSelection.highlight_info.selectContent;
      function listener(e) {
        e.clipboardData.setData("text/html", str);
        e.clipboardData.setData("text/plain", str);
        e.preventDefault();
      }
      document.addEventListener("copy", listener);
      document.execCommand("copy");
      document.removeEventListener("copy", listener);
      this.onCopyText()
    }

    // 构建消息
    this.buildMessage = function (actionType, subActionType, uid) {
      if (actionType == "scroll") {
        return {
          actionType: actionType,
          params: {
            uid: uid,
          },
        };
      }
      let message = {
        content:
          this.tempSelection.highlight_info &&
          this.tempSelection.highlight_info.selectContent,
        actionType: actionType,
        subActionType: subActionType,
        params: {
          ...this.tempSelection.highlight_info,
          actionType: actionType,
          subActionType: subActionType,
        },
      };
      // 提取
      if (actionType == "excerpt_text") {
        let resourceList = [
          {
            content: this.tempSelection.highlight_info.selectContent,
            type: "info",
            createType: 400,
            contentType: 901,
            isDataCollect: 1,
            name: this.tempSelection.highlight_info.selectContent.slice(0, 10),
            origin_page: this.tempSelection.highlight_info.page_num || 1,
            source_info: { ...message.params, preRender: false },
          },
        ];
        message.content = resourceList;
      }
      return message;
    };

    this.getUUID = function () {
      return "xxxx-xxxx-xxxx-xxxx-xxxx".replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0,
          v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      });
    };

    this.getClearWidth = function (width) {
      return parseInt(width.replace("px", "").replace("%", ""));
    };

    // 判断是否hover在文字上
    this.onLayerSpan = function (e) {
      e = e || window.event;
      const path = e.path || (e.composedPath && e.composedPath());

      if (e && path && path.length > 0) {
        if (path[0].nodeName === "SPAN") {
          return true;
        }
      }
      return false;
    };

    // 是否点击在文本区域内
    this.onTextLayer = function (e) {
      return (
        $(e.target).hasClass("textLayer") ||
        $(e.target).parents(".textLayer").length > 0
      );
    };

    this.onSelection = function (e) {
      return (
        $(e.target).hasClass("save-selection") ||
        $(e.target).parents(".save-selection").length > 0
      );
    };

    // sendmessage
    this.onMessage = function (actionType, subActionType, uid, e) {
      let message = this.buildMessage(actionType, subActionType, uid);
      if (!message.params.uid) message.params.uid = this.getUUID();
      if (actionType === "excerpt_text" || actionType === "import_text") {
        message.params.color = undefined;
      }
      let n_message = {};
      n_message.tempSelection = this.tempSelection;
      if (actionType === "highlight") {
        n_message.color = this.quickColor;
      }
      $.extend(true, n_message, message);
      if (actionType === "excerpt_text") {
        if (!!this.tempSelection.highlight_info.color) {
          // 删除高亮
          this.removeHighLight(true);
        }

        this.tempSelection.highlight_info.uid = this.getUUID();
        if(this.cutTextObj) {
          n_message.content[0].content = this.tempSelection.highlight_info.selectContent
        }
      }
      // return
      window.setHighLight = setHighLight;
      if (n_message.actionType === 'highlight') {
        window.parent.handleHighlightMessage(n_message, e, this);
      } else {
        window.parent.handleMessage(n_message, e, this);
      }
    };
    function setHighLight(that) {
      that.onHighlight()
    }
    this.onMouseMove = function (e) {
      if (!this.onClick && e.type != 'touchend') return;
      if (this.hasRectSelected && e.type != 'touchend') {
        this.removeHLMaskEle("temp");
        this.moveCroordsDiv(e);
        return;
      }
      if ($(".pre-selection-warp").length > 0) {
        return;
      }
      const selection = document.getSelection();
      if (
        selection.rangeCount &&
        selection.type === "Range" &&
        !!selection.toString()
      ) {

        this.selectionText = selection.toString();
        const range = selection.getRangeAt(0).cloneRange();
        this.calcRelDom(selection, {type: e.type});
        if (e.type === "mouseup" || e.type === "touchend") {
          this.saveTempRange(selection);
          if(this.quickColor) {
            this.quickLighHight(e)
          } else {
            this.onExcerpt(e);
          }

        }
      }
    };

    this.saveTempRange = function (selection, type, pageNum) {
      this.isDownSelect = false;
      let startNode = this.getNode(selection.anchorNode);
      let endNode = this.getNode(selection.focusNode);
      let startOffset = selection.anchorOffset;

      let endOffset = selection.focusOffset;

      const baseSelector =
        'div[data-page-number="' +
        (pageNum || this.selectionPageNumber) +
        '"] div.textLayer';
      const selector = baseSelector + " span, " + baseSelector + " br";

      const spans = $(selector);

      if (startNode == endNode) {
        if (!$.trim($(startNode).text())) {
          return;
        }
        if(startOffset > endOffset) {
          startOffset = selection.focusOffset
          endOffset = selection.anchorOffset
        }
      } else {
        const spansArr = Array.from(spans)
        const s_index = spansArr.findIndex(v => v == startNode);
        const e_index = spansArr.findIndex(v => v == endNode);
        if(s_index > e_index) {
          startNode = this.getNode(selection.focusNode);
          endNode = this.getNode(selection.anchorNode);
          startOffset = selection.focusOffset
          endOffset = selection.anchorOffset;
          this.isDownSelect = true;
        }
      }


      let index = -1;
      let start = 0;
      let end = 0;
      let s_span = [];
      this.cutTextObj = {}
      let hasStart = false;

      if (startNode == endNode) {
        if (!$.trim($(startNode).text())) {
          return;
        }
        spans.each((i, ele) => {
          if (!!$.trim($(ele).text())) {
            index++;
          }
          if (ele == startNode) {
            start = index;
            end = index;
          }
        });
        s_span.push({
          node: startNode,
          position: this.getTextPosition(startNode, startOffset, endOffset),
        });
      } else {
        spans.each((i, ele) => {
          if (!!$.trim($(ele).text())) {
            index++;
          }
          if (ele == startNode) {
            start = index;
            hasStart = true;
            s_span.push({
              node: ele,
              position: this.getTextPosition(ele, startOffset),
            });
          } else if (ele == endNode) {
            hasStart = false;
            s_span.push({
              node: ele,
              position: this.getTextPosition(ele, undefined, endOffset),
            });
            end = index;
            if (!$.trim($(ele).text())) {
              endOffset = $(spans[i - 1]).text().length;
            }
          } else {
            if (hasStart) {
              s_span.push({
                node: ele,
                position: this.getTextPosition(ele),
              });
            }
          }
        });
      }
      this.tempSelection.highlight_info = {
        ...this.tempSelection.highlight_info,
        selectContent: this.selectionText,
        page_num: this.selectionPageNumber,
        range: {
          start,
          startOffset,
          end,
          endOffset,
        }
      };

      if (type === "calcDom") {
        return s_span;
      }

      if (type === "cutText") {
        return {
          spans: s_span.map(v => v.node),
          startOffset,
          endOffset
        }
      }
    };
    this.getTextWidth = function (fs, ff, str) {
      let canvas =
        this.getTextWidth.canvas ||
        (this.getTextWidth.canvas = document.createElement("canvas"));
      let context = canvas.getContext("2d");
      context.font = fs + "px " + ff;
      var metrics = context.measureText(str);
      return metrics.width;
    };
    this.getTextPosition = function (node, startOffset, endOffset) {
      const j_node = $(node);
      const nodeText = j_node.text();
      let selectStr = nodeText;
      const top = parseFloat(node.style.top);
      let left = parseFloat(node.style.left);
      const f_s = parseFloat(node.style.fontSize);
      const f_f = node.style.fontFamily;
      let scale = 1;
      let matches = node.style.transform.match(/scaleX\((\S*)\)/);
      if (matches && matches.length > 1) {
        scale = matches[1];
      }
      if (typeof startOffset === "number" && typeof endOffset === "number") {
        const min = startOffset > endOffset ? endOffset : startOffset;
        const max = startOffset > endOffset ? startOffset : endOffset;
        selectStr = nodeText.slice(min, max);
      } else if (typeof startOffset === "number") {
        selectStr = nodeText.slice(startOffset);
      } else if (typeof endOffset === "number") {
        selectStr = nodeText.slice(0, endOffset);
      }
      const selectStrWidth = this.getTextWidth(f_s, f_f, selectStr);
      // if (typeof startOffset === "number" && typeof endOffset !== "number") {
      if (typeof startOffset === "number") {
        const offsetStr = nodeText.slice(0, startOffset);
        let offsetStrWidth = this.getTextWidth(f_s, f_f, offsetStr);
        left = left + offsetStrWidth * scale;
      }

      return {
        top,
        left,
        selectStr,
        scale: scale < 1 ? scale : 1,
        fontSize: f_s,
        // height: !$.trim(selectStr) ? f_s : f_s * scale,
        height: !$.trim(selectStr) ? 0 : scale < 1 ? f_s * scale : f_s,
        width: selectStrWidth * scale,
      };
    };

    this.getNode = function (node) {
      if (node.nodeName === "SPAN") {
        return node;
      } else {
        return node.parentNode;
      }
    };

    this.getMinNum = function (no1, no2) {
      return no1 < no2 ? no1 : no2;
    };
    this.getMaxNum = function (no1, no2) {
      return no1 > no2 ? no1 : no2;
    };
    this.calcRelDom = function (selection, options) {
      let lineObj = {};
      let currentLine = 1;

      const relDom = this.saveTempRange(
        selection,
        "calcDom",
        options && options.pageNum
      );
      const length = relDom.length;
      if (length === 1) {
        lineObj[currentLine] = relDom[0].position;
      } else if (length === 2) {
        let f_item = relDom[0].position;
        let e_item = relDom[1].position;
        if (
          Math.abs(f_item.top - e_item.top) <
          this.getMaxNum(f_item.height, e_item.height)
        ) {
          lineObj[currentLine] = {
            top: this.getMaxNum(f_item.top, e_item.top),
            left: f_item.left,
            height: this.getMaxNum(f_item.height, e_item.height),
            width: f_item.width + e_item.width,
            selectStr: f_item.selectStr + e_item.selectStr,
          };
        } else {
          lineObj["1"] = f_item;
          lineObj["2"] = e_item;
        }
      } else {
        // 大于三个dom
        const rowObj = {};
        let line = 1;
        relDom.forEach((item, i) => {
          const l_item = i === 0 ? null : relDom[i - 1].position;
          const n_item = i === length - 1 ? null : relDom[i + 1].position;
          const c_item = item.position;
          if (
            item.node.nodeName === "BR" &&
            relDom[i - 1]?.node.nodeName !== "BR"
          ) {
            line += 1;
          } else if (!$.trim(item.position.selectStr)) {
            line += 1;
          } else if (
            relDom[i - 1]?.node.nodeName !== "BR" &&
            item.node.nodeName !== "BR"
          ) {
            if (i === length - 1) {
              if (
                Math.abs(c_item.top - l_item.top) >
                this.getMaxNum(c_item.height, l_item.height)
              ) {
                line += 1;
              } else {
                if (l_item.left - c_item.left > 50) {
                  line += 1;
                }
              }
            } else if (i > 0 && i < length - 1) {
              if (
                !(
                  (
                    Math.abs(c_item.top - l_item.top) <
                    this.getMaxNum(c_item.height, l_item.height)
                  )
                  // ||
                  // Math.abs(n_item.top - l_item.top) <=
                  //   this.getMaxNum(n_item.height, l_item.height)
                )
              ) {
                line += 1;
              } else if (
                Math.abs(c_item.top - l_item.top) >
                this.getMaxNum(c_item.height, l_item.height) +
                this.getMinNum(c_item.height, l_item.height) / 3
              ) {
                line += 1;
              } else {
                if (l_item.left - c_item.left > 50) {
                  line += 1;
                }
              }
            }
          }

          if (!rowObj[line]) {
            rowObj[line] = [];
          }
          if (item.node.nodeName !== "BR") {
            rowObj[line].push(c_item);
          }
        });
        const _ = this;

        for (const rowObjKey in rowObj) {
          let initialValue = null;
          const rowArr = rowObj[rowObjKey];
          const length = rowArr.length;
          let maxLeft = 0;
          let minLeft = 0;
          let maxItemWidth = 0;
          lineObj[rowObjKey] =
            rowArr.reduce(function (result, currentValue, currentIndex, arr) {
              if (
                (currentIndex === 0 || currentIndex === rowArr.length - 1) &&
                !$.trim(currentValue.selectStr)
              ) {
                return result;
              }

              if (maxLeft > currentValue.left) {
                maxItemWidth = result.width;
              } else {
                maxLeft = currentValue.left;
                maxItemWidth = currentValue.width;
              }
              if (!result) {
                result = { ...currentValue };
                minLeft = currentValue.left;
              } else {
                result.top = _.getMinNum(currentValue.top, result.top);
                result.height = _.getMaxNum(currentValue.height, result.height);
                result.width = currentValue.width + result.width;
                result.selectStr = result.selectStr + currentValue.selectStr;
              }

              minLeft =
                minLeft < currentValue.left ? minLeft : currentValue.left;
              return result;
            }, initialValue) || {};

          const endItem = rowArr[length - 1];
          lineObj[rowObjKey].left = minLeft;
          lineObj[rowObjKey].width = maxLeft - minLeft + maxItemWidth;
        }
      }
      let newLineObj = {};
      let lineNo = 1;
      let lineObjArr = [];
      for (const eleKey in lineObj) {
        lineObjArr.push(lineObj[eleKey]);
      }
      lineObjArr.forEach((v, i) => {
        if (i > 0) {
          let l_item = lineObjArr[i - 1];
          if (
            !(
              Math.abs(v.fontSize - l_item.fontSize) < 2 &&
              Math.abs(v.top - l_item.top) <
              this.getMaxNum(v.height, l_item.height) / 2
            )
          ) {
            if (
              Math.abs(v.fontSize - l_item.fontSize) < 2 &&
              Math.abs(v.top - l_item.top) >
              this.getMaxNum(v.height, l_item.height) / 2
            ) {
              lineNo += 1;
            } else if (
              !(
                Math.abs(v.top - l_item.top) <=
                this.getMaxNum(v.height, l_item.height)
              )
            ) {
              lineNo += 1;
            } else {
              if (
                v.left > l_item.left &&
                (l_item.left + l_item.width > v.left ||
                  v.left - (l_item.left + l_item.width) >
                  l_item.fontSize * l_item.scale)
              ) {
                lineNo += 1;
              }
              if (
                v.left < l_item.left &&
                (v.left + v.width > l_item.left ||
                  l_item.left - (v.left + v.width) > v.fontSize * v.scale)
              ) {
                lineNo += 1;
              }
            }
          }

          // else if (
          //     Math.abs(v.top - l_item.top) > (
          //         this.getMaxNum(v.height, l_item.height) +
          //         this.getMinNum(v.height, l_item.height) / 3
          //     )
          // ) {
          //   lineNo += 1;
          // } else {
          //   if(Math.abs(v.left - l_item.left - l_item.width) > 15) {
          //     lineNo += 1;
          //   }
          // }
        }
        newLineObj[lineNo] = newLineObj[lineNo] || [];
        newLineObj[lineNo].push(v);
      });
      let rowObj = {};
      for (const n_key in newLineObj) {
        let arr = newLineObj[n_key].sort(function (v1, v2) {
          return v1.left - v2.left;
        });

        let length = arr.length;
        let all_h = 0;
        let all_top = 0;
        rowObj[n_key] = arr.reduce(function (
          result,
          currentValue,
          currentIndex,
          arr
          ) {
            if (result) {
              result.selectStr += currentValue.selectStr;
              result.height =
                result.height > currentValue.height
                  ? result.height
                  : currentValue.height;
              result.top =
                result.top > currentValue.top ? result.top : currentValue.top;
            } else {
              result = { ...currentValue };
            }

            all_h += currentValue.height;
            all_top += currentValue.top;

            return result;
          },
          null);
        rowObj[n_key].left = arr[0].left;
        rowObj[n_key].width =
          arr[length - 1].left - arr[0].left + arr[length - 1].width;
        rowObj[n_key].top = all_top / length;
        rowObj[n_key].height = all_h / length;

      }

      $(".new-selection-warp").remove();
      this.createHLMaskEle(rowObj, options);
    };

    /** 创建高亮遮罩层 **/
    this.createHLMaskEle = function (eles, extend = {}) {
      const selector = `.page[data-page-number=${
        extend.pageNum || this.selectionPageNumber
      }] .textLayer`;
      const parentEle = $(selector);
      let className = extend.isSave ? "save-selection" : "new-selection";
      const maskWarp = $(
        '<div id="' + extend.uid + '" class="' + className + '-warp"></div>'
      );
      if (extend.preRender) {
        maskWarp.addClass("pre-selection-warp");
      }

      for (const eleKey in eles) {
        let options = eles[eleKey];
        const { left, top, width, height } = options;

        const isUnderline =
          extend.actionType === "underline" ||
          extend.actionType === "changeUnderline";

        const cssObj = {
          position: "absolute",
          left: left + "px",
          top: top - 1 + "px",
          height: height + 2 + "px",
          width: width + "px",
          backgroundColor: isUnderline
            ? "unset"
            : extend.color || extend.colorDefault || this.quickColor || this.hlDefaultColor,
          opacity: isUnderline ? 1 : 0.3,
          zIndex: extend.isSave ? 9 : -1,
          // zIndex: 9,
          borderRadius: isUnderline ? "unset" : height / 4 + "px",
          // opacity: 0.2
        };

        if (isUnderline) {
          cssObj.borderBottom = "2px solid";
          // cssObj.opacity = 1;
          cssObj.borderColor = extend.color || this.hlDefaultColor;
        }
        if (extend.actionType === "excerpt_text" && !extend.color) {
          cssObj.backgroundColor = "#FFD400";
        }
        if (!!width) {
          const maskEle = $(
            '<div class="hl-mask-selection ' +
            className +
            '" data-uid="' +
            extend.uid +
            '" data-excerptid="' +
            extend.excerptId +
            '"></div>'
          ).css(cssObj);
          maskWarp.append(maskEle);
        }
      }

      parentEle.append(maskWarp);
    };

    // 删除高亮遮罩
    this.removeHLMaskEle = function (type) {

      const uid = this.tempSelection.highlight_info && this.tempSelection.highlight_info.uid
      if (type === "temp") {
        $(".new-selection-warp").remove();
        this.tempSelection = {
          highlight_info: null,
        };
      } else {
        if (uid) {
          $(".save-selection-warp[id = " + uid + "]").remove();
        }
      }


    };

    this.removeHLMaskElePre = function () {
      $(".pre-selection-warp").remove();
    };
    // 获取textLayer位置
    this.getParentPosition = function () {
      const selector = `.page[data-page-number=${this.selectionPageNumber}] .textLayer`;
      const parentEle = $(selector);
      return parentEle[0].getBoundingClientRect();
    };

    this.changeData = function (data) {
      data.forEach((v) => {
        let index = this.highLightData.findIndex(
          (data) => data.highlight_info.uid === v.highlight_info.uid
        );
        if (index > -1) {
          this.highLightData.splice(index, 1, v);
        } else {
          this.highLightData.push(v);
        }
      });
    };

    this.getSelectionDomInfoByRange = function (info) {
      let pageNum = info.highlight_info.page_num;

      if (!pageNum) {
        return
      }
      let textLayers = $(
        'div[data-page-number="' + pageNum + '"] div.textLayer'
      );
      const spans = $(
        'div[data-page-number="' + pageNum + '"] div.textLayer span'
      );
      const notEmpty = [];
      spans.each((i, ele) => {
        if (!!$.trim($(ele).text())) {
          notEmpty.push(ele);
        }
      });
      const start = info.highlight_info.range.start;
      let startOffset = info.highlight_info.range.startOffset;
      let endOffset = info.highlight_info.range.endOffset;
      const end =
        info.highlight_info.range.end > notEmpty.length - 1
          ? notEmpty.length - 1
          : info.highlight_info.range.end;
      const uid = info.highlight_info.uid;
      const startNode = notEmpty[start] && notEmpty[start].childNodes[0];
      const endNode = notEmpty[end] && notEmpty[end].childNodes[0];
      startOffset =
        startOffset > $(startNode).text().length
          ? $(startNode).text().length
          : startOffset;
      endOffset =
        endOffset > $(endNode).text().length
          ? $(endNode).text().length
          : endOffset;
      const range = document.createRange();
      try {
        range.setStart(startNode, startOffset);
        range.setEnd(endNode, endOffset);
      } catch (e) {
        console.log("getSelectionByRange error~~~~~~~~:");
        console.log(info);
        console.log(e);
      }

      let selection = window.getSelection();
      selection.removeAllRanges();
      selection.addRange(range);

      return this.saveTempRange(selection, 'cutText', pageNum)
    }

    // 创建 selection-canvas
    this.createSelectionCanvas = function(pageNum) {
      const selector = '#viewer .page[data-page-number='+ pageNum +'] .canvasWrapper'
      let selectionCanvas = $('#selectionCanvas' + pageNum);
      if(selectionCanvas.length === 0) {
        const textCanvas = $(selector).find('canvas')[0];
        const width = textCanvas.width;
        const height = textCanvas.height;
        selectionCanvas = $('<canvas id="selectionCanvas'+ pageNum +'" width="'+ width +'" height="'+ height +'"></canvas>');
        selectionCanvas.css({
          width, height,
          position: 'absolute',
          top: 0
        })
        $(selector).append(selectionCanvas)
      }
    }
    this.drawSelectionInCanvas = function(eles) {
      const selectionCanvas = $('#selectionCanvas' + this.selectionPageNumber)[0];
      const ctx = selectionCanvas.getContext('2d');
      ctx.clearRect(0, 0, selectionCanvas.width, selectionCanvas.height);
      if(!eles) {
        return
      }

      ctx.fillStyle = 'rgba(11, 0, 255, 0.25)';

      for (const eleKey in eles) {
        const { left, top, width, height } = eles[eleKey];
        const obj = {
          x: left,
          y: top - 1,
          width: width,
          height: height + 2,
        }
        ctx.fillRect(obj.x, obj.y, obj.width, obj.height)
      }

    }
    this.init = function (options = {}) {
      this.options = options;
      this.drawImage = new FirDrawImage()

      PDFViewerApplication.initializedPromise.then(
        function () {
          this.pdfViewerApplication = PDFViewerApplication;
          this.pdfViewer = PDFViewerApplication?.pdfViewer;
          this.pdfViewerApplication.preferences.set("textLayerMode", 1);
          this.pdfViewerApplication.preferences.set('sidebarViewOnLoad', 0);
          this.pdfViewerApplication.preferences.set('defaultZoomValue', 'page-width');



          this.pdfViewer.eventBus.on("pagechanging", (e) => {
            this.lastPage = e.previous;
          });
          this.pdfViewer.eventBus.on("pagerendered", (e) => {
            this.initToolbar();
          });

          this.pdfViewer.eventBus.on("textlayerrendered", (e) => {
            const renderedPageNumber = e.pageNumber;
            this.selectionPageNumber = renderedPageNumber;
            const reRenderArr = this.highLightData.filter((v) => {
              return v.highlight_info.page_num == renderedPageNumber;
            });

            this.reRenderHighLight(reRenderArr);

            // this.resetLink(renderedPageNumber);
            this.createSelectionCanvas(renderedPageNumber)

          });

          this.addEvent();

          if(this.options.hideFunc || this.options.isLeftEdit) {
            $('.highlight-btn').hide()
          }

          window.parent.postMessage(
            {
              actionType: "loadStatus",
              params: {
                status: "end",
              },
            },
            "*"
          );
        }.bind(this)
      );
    };
  };
  SelectionRender.prototype.reRenderHighLight = function (array, isInit, hasSelection) {
    if (isInit) {
      this.changeData(array);
    }
    for (let i = 0; i < array.length; i++) {
      let info = array[i];
      let pageNum = info.highlight_info.page_num;
      if (info.highlight_info.annotationId) {
        continue;
      }
      if (!pageNum) {
        continue;
      }
      let textLayers = $(
        'div[data-page-number="' + pageNum + '"] div.textLayer'
      );
      const spans = $(
        'div[data-page-number="' + pageNum + '"] div.textLayer span'
      );
      const notEmpty = [];
      spans.each((i, ele) => {
        if (!!$.trim($(ele).text())) {
          notEmpty.push(ele);
        }
      });

      try {
        if (textLayers && textLayers.length > 0) {
          let textLayer = textLayers[0];
          if (
            !info.highlight_info.range ||
            notEmpty.length == 0 ||
            (info.highlight_info.rectMode && !info.highlight_info.preRender)
          ) {
            if (info.rect) {
              this.reRenderRectHighLight(info);
            }
            continue;
          }
          let actionType = info.highlight_info.actionType;
          if (actionType == "excerpt_text" || actionType == "import_text") {
            info.highlight_info.color = undefined;
          }
          const start = info.highlight_info.range.start;
          let startOffset = info.highlight_info.range.startOffset;
          let endOffset = info.highlight_info.range.endOffset;
          const end =
            info.highlight_info.range.end > notEmpty.length - 1
              ? notEmpty.length - 1
              : info.highlight_info.range.end;
          const uid = info.highlight_info.uid;
          const startNode = notEmpty[start] && notEmpty[start].childNodes[0];
          const endNode = notEmpty[end] && notEmpty[end].childNodes[0];
          if (!info.highlight_info.color) {
            // 摘录
            startOffset =
              startOffset > $(startNode).text().length
                ? $(startNode).text().length
                : startOffset;
            endOffset =
              endOffset > $(endNode).text().length
                ? $(endNode).text().length
                : endOffset;
          }
          const range = document.createRange();
          try {
            range.setStart(startNode, startOffset);
            range.setEnd(endNode, endOffset);
          } catch (e) {
            console.log("reRenderHighLight error~~~~~~~~:");
            console.log(info);
            console.log(e);
          }
          textLayer.focus()
          let selection = window.getSelection();
          if(!hasSelection) {
            if(/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent)) {
              selection.setBaseAndExtent(startNode, startOffset, endNode, endOffset);
            } else {
              selection.removeAllRanges();
              selection.addRange(range);
            }

          } else if(!this.isPC) {
            selection.removeAllRanges();
            selection.addRange(range);
          }
          this.tempSelection = { ...info };
          $("#" + uid).remove();
          this.calcRelDom(selection, {
            color: info.highlight_info.color,
            isSave: true,
            pageNum: info.highlight_info.page_num,
            uid,
            actionType: info.actionType || info.highlight_info.actionType,
            colorDefault: !info.highlight_info.color ? info.highlight_info.color : undefined,
            preRender: info.highlight_info.preRender,
            excerptId: info.highlight_info.excerptId
          });
          if (!(this.options.isLeftEdit && info.highlight_info.color)) {
            if ('highlight' === info.highlight_info.actionType) {
              this.bindMaskClick(info.highlight_info, info.highlight_info.actionType);
            }
          }
        }
      } catch (e) {
        console.log("reRenderHighLight error~~~~~~~~:");
        console.log(info);
        console.log(e);
      }
    }
    if (isInit) {
      this.tempSelection.highlight_info = {};
      this.initToolbar();
    }
  };

  SelectionRender.prototype.clearRenderHighLight = function (array) {
    for (let i = 0; i < array.length; i++) {
      let uid = array[i].uid;
      if (uid) {
        this.removeHighLightData(uid);
        $('.select-tag-warp').remove();
        $("#" + uid).remove();
        this.hideToolbar()
      };
    }
  };

  // 跳转到某个高亮
  SelectionRender.prototype.scrollToHighLight = function (data) {
    this.pdfViewerApplication.pdfViewer.currentPageNumber = +data.page_num;
    if (data.annotationId) {
      setTimeout(() => {
        $(
          `section[data-annotation-id="${data.annotationId}"]`
        )[0].scrollIntoView();
      }, 300);
      return;
    }
    const startSign = $(`<img src="images/icon-start.svg" class="sign" />`);
    const endSign = $(`<img src="images/icon-end.svg" class="sign" />`);
    setTimeout(() => {
      let scrollTop =
        $("#viewerContainer").scrollTop() +
        $("div[data-uid='" + data.uid + "']").position().top -
        50;
      $("#viewerContainer").animate(
        {
          scrollTop: scrollTop,
        },
        50
      );
    }, 300);
    $("#"+ data.uid +" .save-selection").css({ opacity: 0.3 });
    $("#" + data.uid)
      .children()
      .animate({ opacity: 0.8 }, 1000)
      .animate({ opacity: 0.3 }, 1000);

    $(`.rect[id="${data.uid}"]`)
      .animate({ opacity: 0.8 }, 1000)
      .animate({ opacity: 0.3 }, 1000);

    let selections = $(`.save-selection[data-uid="${data.uid}"]`);
    $(".sign").remove();
    if (selections && selections.length > 0) {
      let cssObj = {
        height: $(selections[0]).height(),
        top: parseFloat($(selections[0]).css("top")) - 5,
        left: parseFloat($(selections[0]).css("left")) - 5,
      };

      let lastSelection = selections[selections.length - 1];
      let endCssObj = {
        height: $(lastSelection).height(),
        top: parseFloat($(lastSelection).css("top")) + 4,
        left:
          parseFloat($(lastSelection).css("left")) -
          parseFloat($(lastSelection).height()) +
          parseFloat($(lastSelection).width()) +
          4,
      };

      startSign.css(cssObj);
      endSign.css(endCssObj);
      $(`.save-selection-warp[id="${data.uid}"]`).append(startSign);
      $(`.save-selection-warp[id="${data.uid}"]`).append(endSign);
    }
  };

  // 框选结果渲染
  SelectionRender.prototype.reRenderRectHighLight = function (info) {
    let textLayers = $(
      'div[data-page-number="' + info.page_num + '"] div.textLayer'
    );
    if (textLayers && textLayers.length > 0) {
      $("#" + info.uid).remove();
      let textLayer = textLayers[0];
      let ph = textLayer.style.height.replace("px", "");
      let pw = textLayer.style.width.replace("px", "");
      let div = document.createElement("div");
      div.id = info.uid;
      div.style.top = parseFloat(info.rect.top) * 0.01 * ph + "px";
      div.style.left = parseFloat(info.rect.left) * 0.01 * pw + "px";
      div.style.height = parseFloat(info.rect.height) * ph + "px";
      div.style.width = parseFloat(info.rect.width) * pw + "px";
      div.className = "rect";
      div.setAttribute("data-uid", info.uid);
      textLayer.appendChild(div);
    }
  };
})(window);
