(function() {

    window.FirDrawImage = function() {
        this.ctx = null;
        this.canvas = null;
        this.pageEle = null;
        this.$pageEle = null;
        this.canvasWrapper = null;
        this.position = {
            sx: 0,
            sy: 0,
            ex: 0,
            ey: 0
        }
        this.prePosition = {
            x: this.position.sx,
            y: this.position.sy,
            width: 0, 
            height: 0
        }

        this.changePosition = null;
        
        this.canvasLeft = 0;
        this.canvasTop = 0;

        this.dots = [];
        
        this.isDrawingRect = false;
        this.isDrawingDot = false;
        this.isShowRect = false;

        this.cursor = null;
        this.dir = null;
        this.pageNo = 0;

        this.selectCb = null;
        this.selectCancelCb = null;

        this.createCanvas = function(pageNo, e) {
            this.pageNo = pageNo;
            
            this.canvasWrapper = $('.drawWrapper');
            if(this.canvasWrapper.length > 0) {
                this.canvasWrapper.remove()
            }
            this.$pageEle = $('.page[data-page-number="'+ pageNo +'"]')
            this.pageEle = this.$pageEle[0];
            const pageCanvas = $('.page[data-page-number="'+ pageNo +'"] .canvasWrapper canvas')[0];
            
            this.canvasWrapper = $('<div class="drawWrapper"></div>');
            canvasEle = $('<canvas id="drawCanvas"></canvas>');
            this.canvas = canvasEle[0]
            const width = pageCanvas.width;
            const height = pageCanvas.height;
            this.canvas.width = width;
            this.canvas.height = height;
            this.canvasWrapper.append(this.canvas)
            this.canvasWrapper.css({
                width, height,
                position: 'absolute',
                left: 0,
                top: 0,
                zIndex: 1
            })
            $('.page[data-page-number="'+ pageNo +'"]').append(this.canvasWrapper);

            const clickX = e.offsetX;
            const clickY = e.offsetY;

            this.position.sx = clickX;
            this.position.sy = clickY;
            this.ctx = this.canvas.getContext('2d');

            // this.ctx.scale(2, 2)

            const {left, top} = this.canvas.getBoundingClientRect();
            this.canvasLeft = left;
            this.canvasTop = top;
            const clickPosition = this.getPosition(e);
            this.position.sx = clickPosition.x;
            this.position.sy = clickPosition.y;

            this.isDrawingRect = true;
            this.$pageEle.on('mousemove', this.moveRect.bind(this));
            this.$pageEle.on('mouseup', this.cancelDrawRect.bind(this));
        }

        this.getPosition = function(e) {
            return {
                x: e.pageX - this.canvasLeft,
                y: e.pageY - this.canvasTop
            }
        }

        this.getMinNum = function(a, b) {
            return a < b ? a : b;
        }

        this.getPositionInCanvas = function(e) {
            const mousePosition = this.getPosition(e);
            let x = mousePosition.x;
            let y = mousePosition.y;

            if(y < (e.offsetY + 1)) {
                y = e.offsetY;
            }

            if(x < 0) {
                x = 1;
            }
            if(x > this.canvas.width) {
                x = this.canvas.width - 1;
            }

            return {x, y}
        }

        this.moveRect = function(e) {

            const mouseCanvasPosition = this.getPositionInCanvas(e)
            this.position.ex = mouseCanvasPosition.x;
            this.position.ey = mouseCanvasPosition.y;

            const dx = this.getMinNum(this.position.sx, this.position.ex);
            const dy = this.getMinNum(this.position.sy, this.position.ey);
            const width = Math.abs(this.position.sx - this.position.ex);
            const height = Math.abs(this.position.sy - this.position.ey);

            this.drawRect(dx, dy, width, height);
            
        }

        this.drawRect = function(dx, dy, width, height) {
            this.prePosition = {
                x: dx,
                y: dy,
                width, height
            }
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.ctx.strokeStyle = '#FFD400';
            this.ctx.lineWidth = 1;
            this.ctx.strokeRect(dx, dy+ 0.5, width, height)
        }

        this.drewDot = function() {
            this.isDrawingDot = true;
            const {x, y, width, height} = this.prePosition;
            const half_w = width / 2;
            const half_h = height / 2;
           
            this.dots = [];
            this.dots.push({x, y}); // 上左
            this.dots.push({x: x + half_w, y}); // 上中
            this.dots.push({x: x + width, y}); // 上右

            this.dots.push({x, y: y + half_h}); // 中左
            this.dots.push({x: x + width, y: y + half_h}); // 中右

            this.dots.push({x, y: y + height}); // 下左
            this.dots.push({x: x + half_w, y: y + height}); // 下中
            this.dots.push({x: x + width, y: y + height}); // 下右

            this.ctx.lineWidth = 1;

            this.dots.forEach((item) => {
                this.ctx.beginPath();
                this.ctx.arc(item.x, item.y + 0.5, 3, 0, Math.PI * 2, true);
                this.ctx.fillStyle="#fff";
                this.ctx.fill();
                this.ctx.strokeStyle="#FFD400";
                this.ctx.stroke();
            })
            
            this.isShowRect = true;
            this.canvasWrapper.css({
                zIndex: 9999
            })
            this.canvasBindEvents()
        }

        this.getPositionInRange = function(pathX, pathY, width, height, x, y) {
            return (
                x >= pathX && y >= pathY && 
                x <= pathX + width && 
                y <= pathY + height
            )
        }

        this.getPositionInDotRange = function(cx, cy, radius, x, y) {

            const pathX = cx - radius;
            const pathY = cy - radius;
            const width = radius * 2;
            const height = radius * 2;
            return this.getPositionInRange(pathX, pathY, width, height, x, y)
        }

        this.getCursor = function(e) {
            let cursor = 'auto';
            let dir = null;
            const {x, y} = this.getPositionInCanvas(e)
                

            if(this.getPositionInRange(this.prePosition.x, this.prePosition.y, 
                this.prePosition.width, this.prePosition.height, x, y)) {
                cursor = 'move'; // 移动
                dir = 'move';
            }

            this.dots.forEach((v, i) => {
                if(this.getPositionInDotRange(v.x, v.y, 4, x, y)) {
                    if(i === 0) {
                        cursor = 'nwse-resize'; // 缩放
                        dir = 'lt';
                    }
                    if(i === 1) {
                        cursor = 'ns-resize'; // 缩放
                        dir = 'top';
                    }
                    if(i === 2) {
                        cursor = 'nesw-resize'; // 缩放
                        dir = 'rt';
                    }
                    if(i === 3) {
                        cursor = 'ew-resize'; // 缩放
                        dir = 'left';
                    }
                    if(i === 4) {
                        cursor = 'ew-resize'; // 缩放
                        dir = 'right';
                    }
                    if(i === 5) {
                        cursor = 'nesw-resize'; // 缩放
                        dir = 'lb';
                    }
                    if(i === 6) {
                        cursor = 'ns-resize'; // 缩放
                        dir = 'bottom';
                    }
                    if(i === 7) {
                        cursor = 'nwse-resize'; // 缩放
                        dir = 'rb';
                    }
                    // if(i === 0 || i === 7) {
                    //     cursor = 'nwse-resize'; // 缩放
                    // }
                    // if(i === 1 || i === 6) {
                    //     cursor = 'ns-resize'; // 上下
                    // }

                    // if(i === 2 || i === 5) {
                    //     cursor = 'nesw-resize'; // 缩放
                    // }

                    // if(i === 3 || i === 4) {
                    //     cursor = 'ew-resize'; // 左右
                    // }
                }
                
            })

            return {
                cursor, x, y, dir
            };
        }

        this.changeRectposition = function(endX, endY) {
            let {x, y, width, height} = this.prePosition;

            let offsetX = endX - this.changePosition.startX;
            let offsetY = endY - this.changePosition.startY;
            if(this.dir === 'move') {
                x += offsetX;
                y += offsetY;

                this.changePosition.startX = endX;
                this.changePosition.startY = endY;
            }

            if(this.dir === 'lt') {
                offsetX = offsetX > width ? width : offsetX;
                offsetY = offsetY > height ? height : offsetY;
                x += offsetX;
                y += offsetY;
                width = width - offsetX;
                height = height - offsetY;
                this.changePosition.startX = x;
                this.changePosition.startY = y;
            }

            if(this.dir === 'top') {
                offsetY = offsetY > height ? height : offsetY;
                y += offsetY;
                height = height - offsetY;
                this.changePosition.startY = y;
            }

            if(this.dir === 'rt') {
                if(offsetX < 0 && Math.abs(offsetX) > width) {
                    offsetX = -width
                }
                offsetY = offsetY > height ? height : offsetY;
                
                y += offsetY;
                width += offsetX;
                height -= offsetY;
                this.changePosition.startX = endX;
                this.changePosition.startY = endY;
            }

            if(this.dir === 'left') {
                offsetX = offsetX > width ? width : offsetX;
                x += offsetX;  
                width -= offsetX;
                this.changePosition.startX = endX;
            }

            if(this.dir === 'right') {
                if(offsetX < 0 && Math.abs(offsetX) > width) {
                    offsetX = -width
                }     
                width += offsetX;
                this.changePosition.startX = endX;
            }

            if(this.dir === 'lb') {
                offsetX = offsetX > width ? width : offsetX;
                x += offsetX;
                width -= offsetX;
                height += offsetY;
                width = width < 0 ? 0 : width;
                height = height < 0 ? 0 : height;
                this.changePosition.startX = x;
                this.changePosition.startY = y + height;
            }

            if(this.dir === 'bottom') {
                height += offsetY;
                height = height < 0 ? 0 : height;
                this.changePosition.startY = y + height;
            }

            if(this.dir === 'rb') {
                if(offsetX < 0 && Math.abs(offsetX) > width) {
                    offsetX = -width
                }   
                width += offsetX;
                height += offsetY;
                height = height < 0 ? 0 : height;
                this.changePosition.startX = endX;
                this.changePosition.startY = y + height;
            }

            

            if((x + width) > this.canvas.width) {
                x = this.canvas.width - width - 2
            }
            if(x < 2) {
                x = 2
            }
            if(y < 0) {
                y = 0
            }
            if(y > this.canvas.height) {
                y = this.canvas.height
            }


            this.drawRect(x, y, width, height);
            
        }

        this.timeMove = function(e) {
            const {x, y, cursor} = this.getCursor(e);

            if(this.isShowRect && !!this.changePosition) {
                
                this.changeRectposition(x, y)
            } else {
                
                $('body').css({cursor})
            }
        }

        this.cancelRectRange = function() {
            this.canvasWrapper?.css({
                zIndex: 1
            });
            this.ctx?.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.changePosition = null;
            this.cursor = 'auto';
            $('body').css({cursor: 'auto'})
            this.position = {
                sx: 0,
                sy: 0,
                ex: 0,
                ey: 0
            }
            this.prePosition = {
                x: this.position.sx,
                y: this.position.sy,
                width: 0, 
                height: 0
            }
        }

        this.canvasBindEvents = function() {
            this.canvasWrapper.off()
            this.canvasWrapper.on('mousemove', this.timeMove.bind(this))

            this.canvasWrapper.on('mouseup', function(e) {
                if(this.cursor !== 'auto') {
                    this.drewDot();
                    if(this.prePosition.width > 0 && this.prePosition.height > 0) {
                        this.selectCb(this.prePosition);
                    }
                    
                }
                this.cursor = 'atuo';
                this.changePosition = null;
            }.bind(this))


            this.canvasWrapper.on('mousedown', function(e) {
                const {x, y, cursor, dir} = this.getCursor(e);
                if(cursor === 'auto') {
                    // 删除框选区域 重置数据
                    this.cancelRectRange()
                    this.selectCancelCb()
                } else {
                    this.cursor =  cursor;
                    this.dir =  dir;
                    this.changePosition = {
                        startX: x,
                        startY: y
                    }
                }

            }.bind(this))
        }

        this.cancelDrawRect = function() {
            this.isDrawingRect = false;
            this.$pageEle.off();
            this.drewDot();
            this.selectCb(this.prePosition);
        }
    
        this.init = function(pageNo, e, onRender, onCancel) {
            this.selectCb = onRender;
            this.selectCancelCb = onCancel;
            this.createCanvas(pageNo, e);
            // 鼠标移除canvas范围松开鼠标
            $(window).on('mouseup', function() {
                if(this.isDrawingRect) {
                    this.cancelDrawRect()
                }
            }.bind(this))
        }
    }
    
})(window);