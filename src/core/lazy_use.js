import Vue from 'vue'

// base library
import {
  FormModel,
  Affix,
  Rate,
  Timeline,
  Comment,
  Empty,
  Anchor,
  Tree,
  ConfigProvider,
  Carousel,
  Layout,
  Input,
  InputNumber,
  Button,
  Switch,
  Radio,
  Checkbox,
  Select,
  Card,
  Form,
  Row,
  Col,
  Modal,
  Table,
  Tabs,
  Icon,
  Badge,
  Popover,
  Dropdown,
  List,
  Avatar,
  Breadcrumb,
  Steps,
  Spin,
  Menu,
  Drawer,
  Tooltip,
  Alert,
  Tag,
  Divider,
  DatePicker,
  TimePicker,
  Upload,
  Progress,
  Skeleton,
  Popconfirm,
  PageHeader,
  Result,
  Statistic,
  Descriptions,
  message,
  notification,
  AutoComplete, Slider
} from 'ant-design-vue'
import Viser from 'viser-vue'

// ext library
import VueCropper from 'vue-cropper'
import Dialog from '@/components/Dialog'
import MultiTab from '@/components/MultiTab'
import PageLoading from '@/components/PageLoading'
import PermissionHelper from '@/core/permission/permission'
import './directives/action'
import VCharts from 'v-charts'
import UserImg from '@/views/business/components/NCardList'
import LightTimeline from 'vue-light-timeline'
import infiniteScroll from 'vue-infinite-scroll'
import VueImageSwipe from 'vue-image-swipe'
import 'vue-image-swipe/dist/vue-image-swipe.css'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import Organization from 'highcharts/modules/organization'
import Sankey from 'highcharts/modules/sankey'
import Exporting from 'highcharts/modules/exporting'
import Accessibility from 'highcharts/modules/accessibility'
Sankey(Highcharts)
Exporting(Highcharts)
Accessibility(Highcharts)
Organization(Highcharts)
import VueClipboard from 'vue-clipboard2'
import VDistpicker from 'v-distpicker'
import { showImages } from 'vue-img-viewr'
import 'vue-img-viewr/styles/index.css'

Vue.use(FormModel);
Vue.use('v-distpicker',VDistpicker)
Vue.use(Affix)
Vue.use(HighchartsVue)
VueClipboard.config.autoSetContainer = true
Vue.use(VueClipboard)
Vue.use(VueImageSwipe)
Vue.use(infiniteScroll)
Vue.use(LightTimeline)
Vue.use(Slider)
Vue.use(Rate)
Vue.use(Timeline)
Vue.use(Comment)
Vue.use(Empty)
Vue.use(Anchor)
Vue.use(Tree)
Vue.use(ConfigProvider)
Vue.use(Carousel)
Vue.use(Layout)
Vue.use(Input)
Vue.use(InputNumber)
Vue.use(Button)
Vue.use(Switch)
Vue.use(Radio)
Vue.use(Checkbox)
Vue.use(Select)
Vue.use(Card)
Vue.use(Form)
Vue.use(Row)
Vue.use(Col)
Vue.use(Modal)
Vue.use(Table)
Vue.use(Tabs)
Vue.use(Icon)
Vue.use(Badge)
Vue.use(Popover)
Vue.use(Dropdown)
Vue.use(List)
Vue.use(Avatar)
Vue.use(Breadcrumb)
Vue.use(Steps)
Vue.use(Spin)
Vue.use(Menu)
Vue.use(Drawer)
Vue.use(Tooltip)
Vue.use(Alert)
Vue.use(Tag)
Vue.use(Divider)
Vue.use(DatePicker)
Vue.use(TimePicker)
Vue.use(Upload)
Vue.use(Progress)
Vue.use(Skeleton)
Vue.use(Popconfirm)
Vue.use(PageHeader)
Vue.use(Result)
Vue.use(Statistic)
Vue.use(Descriptions)
Vue.use(AutoComplete)
Vue.use(VCharts)
Vue.use(UserImg)

Vue.prototype.$confirm = Modal.confirm
Vue.prototype.$message = message
Vue.prototype.$notification = notification
Vue.prototype.$info = Modal.info
Vue.prototype.$success = Modal.success
Vue.prototype.$error = Modal.error
Vue.prototype.$warning = Modal.warning
Vue.prototype.$showImages = showImages

Vue.use(Viser)
Vue.use(Dialog) // this.$dialog func
Vue.use(MultiTab)
Vue.use(PageLoading)
Vue.use(PermissionHelper)
Vue.use(VueCropper)

process.env.NODE_ENV !== 'production' && console.warn('[antd-pro] NOTICE: Antd use lazy-load.')
