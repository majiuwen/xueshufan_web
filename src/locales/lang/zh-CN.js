import antd from 'ant-design-vue/es/locale-provider/zh_CN'
import momentCN from 'moment/locale/zh-cn'

const components = {
  antLocale: antd,
  momentName: 'zh-cn',
  momentLocale: momentCN
}

const locale = {
  'message': '-',
  'menu.home': '首页',
  'menu.user': '用户',
  'menu.user.home': '用户主页',
  'menu.user.UserDynamic':'用户动态',
  'menu.user.UserSummary':'用户概况',
  'menu.user.UserAchievements':'用户学术成果',
  'menu.user.UserClaimArticles':'用户认领文章',
  'menu.user.UserManageAccess':'用户管理全文访问权限',
  'menu.user.UserGenerateReport':'用户生成报告',
  'menu.user.UserStatistics':'用户统计数字',
  'menu.user.UserExperience':'用户经历',
  'menu.user.UserFavorites':'用户收藏',
  'menu.user.EditProfile':'个人资料',

  'menu.searchResults.searchResultsDirectionDetail':'研究方向详细',

  'menu.articles': '文献',
  'menu.articles.ArticlesFullText':'文献全文',
  'menu.articles.ArticlesReference':'参考文献',
  'menu.articles.ArticlesCitation':'引证文献',
  'menu.articles.ArticlesStatistics':'文献统计',
  'menu.articles.ArticlesDiscuss':'文献讨论',
  'menu.articles.ArticlesShare':'文献分享',

  'menu.scholar': '学者',
  'menu.scholar.ScholarSummary':'学者概述',
  'menu.scholar.ScholarAchievements':'学者学术成果',
  'menu.scholar.ScholarStatistics':'学者统计数字',
  'menu.scholar.ScholarExperience':'学者经历',

  'menu.institutions': '学术机构',
  'menu.institutions.InstitutionsSummary':'学术概况',
  'menu.institutions.InstitutionsAchievements':'学术成果',
  'menu.institutions.InstitutionsDepartment':'院系',
  'menu.institutions.InstitutionsMember':'会员',
  'menu.institutions.InstitutionsMemberStatistics':'会员统计',

  'menu.notification': '消息',
  'menu.notification.NotificationUpdates':'更新',
  'menu.notification.NotificationMessages':'私信',
  'menu.notification.NotificationRequests':'全文请求',

  'menu.searchResults': '检索结果',
  'menu.searchResults.SearchResultsScholar':'学者',
  'menu.searchResults.SearchResultsArticles':'文献',
  'menu.searchResults.SearchResultsDirection':'研究方向',
  'menu.searchResults.SearchResultsInstitutions':'研究机构',
  'menu.searchResults.Department':'院系',

  'menu.talentMap':'人才地图',

  'menu.newsDetails':'详细动态',

  'layouts.usermenu.dialog.title': '用户注销',
  'layouts.usermenu.dialog.content': '确认退出登录吗？',

  'layouts.managemenu.dialog.title': '后台系统注销',
  'layouts.managemenu.dialog.content': '确认退出后台系统吗？',

  'layouts.delNews.dialog.title': '动态删除',
  'layouts.delNews.dialog.content': '确认删除这条动态吗？',

  'layouts.delNewsConment.dialog.title': '删除评论',
  'layouts.delNewsConment.dialog.content': '是否确定删除该评论？',
}

export default {
  ...components,
  ...locale
}
