import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import LoginStore from '@/views/business/userLogin/login/LoginStore.js'

// default router permission control
import permission from './modules/permission'

// dynamic router permission control (Experimental)
// import permission from './modules/async-router'
import getters from './getters'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    LoginStore,
    permission
  },
  state: {

  },
  mutations: {

  },
  actions: {

  },
  getters
})
