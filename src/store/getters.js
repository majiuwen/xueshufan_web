const getters = {
  isMobile: state => state.app.isMobile,
  lang: state => state.app.lang,
  theme: state => state.app.theme,
  color: state => state.app.color,
  loginVisible: state => state.app.loginVisible,
  searchBar: state => state.app.searchBar,
  userId: state => state.LoginStore.userId,
  token: state => state.LoginStore.token,
  avatar: state => state.LoginStore.avatar,
  nickname: state => state.LoginStore.name,
  welcome: state => state.LoginStore.welcome,
  roles: state => state.LoginStore.roles,
  userInfo: state => state.LoginStore.info,
  addRouters: state => state.permission.addRouters,
  multiTab: state => state.app.multiTab
}

export default getters
