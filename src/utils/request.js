import axios from 'axios'
import store from '@/store'
import storage from 'store'
import notification from 'ant-design-vue/es/notification'
import { VueAxios } from './axios'
import { ACCESS_TOKEN } from '@/store/mutation-types'

// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 60000 // 请求超时时间
})

// 异常拦截处理器
const errorHandler = (error) => {
  if (error.response) {
    const data = error.response.data
    // 从 localstorage 获取 token
    const token = storage.get(ACCESS_TOKEN)
    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: data.message
      })
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      notification.error({
        message: 'Unauthorized',
        description: 'Authorization verification failed'
      })
      if (token) {
        store.dispatch('Logout').then(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    }
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {
  const token = storage.get(ACCESS_TOKEN)
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  if (token) {
    config.headers['Authorization'] = token
  }
  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use((response) => {
  if(response.config.responseType){
    if(response.config.responseType === 'blob'){
      if(response.data.type === 'application/json'){
        let reader = new FileReader();
        reader.readAsText(response.data,"utf-8");
        reader.onload = function(e){
          let jsonData = JSON.parse(reader.result)
          if(jsonData.ret_code.substring(0,3) === '201'){
            if(jsonData.ret_code === '20101' || jsonData.ret_code === '20102') {
              store.dispatch('Logout').then(() => {
                store.dispatch('changeVisible', true)
              })
            }
          }
        }
      }
    }
  }else {
    if(response.data.ret_code){
      if(response.data.ret_code.substring(0,3) === '201'){
        if(response.data.ret_code === '20101' || response.data.ret_code === '20102') {
          store.dispatch('Logout').then(() => {
            store.dispatch('changeVisible', true)
          })
        }
        return Promise.reject(response.data)
      }

      if(response.data.ret_code !== '0'){
        if(response.data){
          if(response.data.ret_msg){
            notification.info({
              message: response.data.ret_msg
            })
          }
          return Promise.reject(response.data)
        }else {
          return Promise.reject()
        }
      }
    }

  }
  return response.data
}, errorHandler)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, request)
  }
}

export default request

export {
  installer as VueAxios,
  request as axios
}
