import request from '@/utils/request'

const api = {
  getUserExcerpt: '/userExcerpt/getUserExcerpt',
  insertUserExcerpt: '/userExcerpt/insertUserExcerpt',
  deleteUserExcerpt: '/userExcerpt/deleteUserExcerpt',
  updateUserExcerpt: '/userExcerpt/updateUserExcerpt',
  insertUserAnnotation: '/userAnnotation/insertUserAnnotation',
  deleteUserAnnotation: '/userAnnotation/deleteUserAnnotation',
  updateUserAnnotation: '/userAnnotation/updateUserAnnotation',
  getUserTranslationToolList: '/userTranslationTool/getUserTranslationToolList',
}

/**
 * 获取用户摘录信息
 * @param parameter
 * @returns {*}
 */
export function selUserExcerptArray (parameter) {
  return request({
    url: api.getUserExcerpt,
    method: 'post',
    data: parameter
  })
}

/**
 * 新增用户摘录信息
 * @param parameter
 * @returns {*}
 */
export function insertUserExcerpt (parameter) {
  return request({
    url: api.insertUserExcerpt,
    method: 'post',
    data: parameter
  })
}

/**
 * 删除用户摘录信息
 * @param parameter
 * @returns {*}
 */
export function deleteUserExcerpt (parameter) {
  return request({
    url: api.deleteUserExcerpt,
    method: 'post',
    data: parameter
  })
}

/**
 * 更新用户摘录信息
 * @param parameter
 * @returns {*}
 */
export function updateUserExcerpt (parameter) {
  return request({
    url: api.updateUserExcerpt,
    method: 'post',
    data: parameter
  })
}

/**
 * 新增用户批注信息
 * @param parameter
 * @returns {*}
 */
export function insertUserAnnotation (parameter) {
  return request({
    url: api.insertUserAnnotation,
    method: 'post',
    data: parameter
  })
}

/**
 * 删除用户批注信息
 * @param parameter
 * @returns {*}
 */
export function deleteUserAnnotation (parameter) {
  return request({
    url: api.deleteUserAnnotation,
    method: 'post',
    data: parameter
  })
}

/**
 * 更新用户批注信息
 * @param parameter
 * @returns {*}
 */
export function updateUserAnnotation (parameter) {
  return request({
    url: api.updateUserAnnotation,
    method: 'post',
    data: parameter
  })
}

/**
 * 获取翻译工具信息
 * @param parameter
 * @returns {*}
 */
export function getUserTranslationToolList (parameter) {
  return request({
    url: api.getUserTranslationToolList,
    method: 'post',
    data: parameter
  })
}