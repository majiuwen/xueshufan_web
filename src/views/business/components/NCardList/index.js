import MyUserImg from './UserImg.vue'
const UserImg = {
  install: function(Vue){
    Vue.component('UserImg',MyUserImg)
  }
}
export default UserImg;