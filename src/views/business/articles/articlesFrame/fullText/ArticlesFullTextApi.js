import request from '@/utils/request'

const api = {
  getArticlesFullTextList: '/articlesFullText/getPublicationDataByPublicationId',
}

/**
 * 根据文献ID取得文献信息
 * @param parameter
 * @returns {*}
 */
export function getArticlesFullTextList(parameter) {
  return request({
    url: api.getArticlesFullTextList,
    method: 'post',
    data: parameter
  })
}

/**
 * 判断pdf画布是否显示成功
 * @param canvas
 * @returns {boolean}
 */
export function isCanvasBlank(canvas) {
  let blank = document.createElement('canvas');
  blank.width = canvas.width;
  blank.height = canvas.height;

  return canvas.toDataURL() === blank.toDataURL();
}

/**
 * 函数节流
 * @param fn
 * @param interval
 * @returns {function(): (undefined)}
 */
export function throttle(fn, interval = 300) {
  let canRun = true;
  return function () {
    if (!canRun) return;
    canRun = false;
    setTimeout(() => {
      fn.apply(this, arguments);
      canRun = true;
    }, interval);
  }
}