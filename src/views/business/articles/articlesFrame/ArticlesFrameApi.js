import request from '@/utils/request'

const api = {
  getRelatedPublication: '/articlesFullText/getRelatedPublication',
  getPublicationReferenceCount: '/articlesReference/getPublicationReferenceCount',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getRelatedPublication (parameter) {
  return request({
    url: api.getRelatedPublication,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getPublicationReferenceCount (parameter) {
  return request({
    url: api.getPublicationReferenceCount,
    method: 'post',
    data: parameter
  })
}