import request from '@/utils/request'

const api = {
  getPublicationInfoByPublicationId: '/articlesFullText/getPublicationInfoByPublicationId',
  getAbstractTranslation:'/abstractTranslation/getAbstractTranslation',
}
/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getPublicationInfoByPublicationId (parameter) {
  return request({
    url: api.getPublicationInfoByPublicationId,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getAbstractTranslation (parameter) {
  return request({
    url: api.getAbstractTranslation,
    method: 'post',
    data: parameter
  })
}