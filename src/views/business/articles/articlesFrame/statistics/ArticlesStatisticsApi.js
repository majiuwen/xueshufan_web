import request from '@/utils/request'

const api = {
  getStatsPublicationYearlyList: '/statsLiterature/getStatsLiterature',
  getStatsPublicationList: '/statsLiterature/getStatsPublicationTotal'
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getStatsPublicationYearlyList (parameter) {
  return request({
    url: api.getStatsPublicationYearlyList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getStatsPublicationList (parameter) {
  return request({
    url: api.getStatsPublicationList,
    method: 'post',
    data: parameter
  })
}

