import request from '@/utils/request'

const api = {
  getUserMaker:'/talentMap/getUserMaker',
  getCountryNameList:'/talentMap/getCountryNameList',
  getUserRelation:'/talentMap/getUserRelation',
  getAllRelation:'/talentMap/getAllRelation',
  getUserRelationNode:'/talentMap/getUserRelationNode',
  getUserCheckRelation:'/talentMap/getUserCheckRelation',
  getUserCheckRelationNode:'/talentMap/getUserCheckRelationNode',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserMaker (parameter) {
  return request({
    url: api.getUserMaker,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getCountryNameList (parameter) {
  return request({
    url: api.getCountryNameList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserRelation (parameter) {
  return request({
    url: api.getUserRelation,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getAllRelation (parameter) {
  return request({
    url: api.getAllRelation,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserRelationNode (parameter) {
  return request({
    url: api.getUserRelationNode,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserCheckRelation (parameter) {
  return request({
    url: api.getUserCheckRelation,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserCheckRelationNode (parameter) {
  return request({
    url: api.getUserCheckRelationNode,
    method: 'post',
    data: parameter
  })
}