import request from '@/utils/request'

const api = {
  getSuggestedPublicationToRead: '/userHome/getSuggestedPublicationToRead',
  publicationBeingRead: '/userHome/publicationBeingRead',
  getThisWeekStatistics: '/userHome/getThisWeekStatistics',
  getAnnouncementList: '/userHome/getAnnouncementList',
  saveNewDevelopments: 'userNewDevelopments/saveNewDevelopments',
  getOtherNewDevelopments: 'userHome/getOtherNewDevelopments',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getSuggestedPublicationToRead (parameter) {
  return request({
    url: api.getSuggestedPublicationToRead,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function publicationBeingRead (parameter) {
  return request({
    url: api.publicationBeingRead,
    method: 'post',
    data: parameter
  })
}
/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getThisWeekStatistics (parameter) {
  return request({
    url: api.getThisWeekStatistics,
    method: 'post',
    data: parameter
  })
}
/**
 * 获取各类通知，包括广告
 * @param parameter
 * @returns {*}
 */
export function getAnnouncementList (parameter) {
  return request({
    url: api.getAnnouncementList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function saveNewDevelopments (parameter) {
  return request({
    url: api.saveNewDevelopments,
    method: 'post',
    data: parameter,
    contentType : 'application/json; charset=UTF-8'
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getOtherNewDevelopments (parameter) {
  return request({
    url: api.getOtherNewDevelopments,
    method: 'post',
    data: parameter
  })
}
