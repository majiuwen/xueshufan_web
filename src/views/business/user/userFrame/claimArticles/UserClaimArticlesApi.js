import request from '@/utils/request'

const api = {
  GetBookList: '/UserClaimArticles/GetBookList',
  UpdatePublicationUser: '/UserClaimArticles/UpdatePublicationUser',
  UpdateSuggestedPublicationToClaim: '/UserClaimArticles/UpdateSuggestedPublicationToClaim',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function GetBookList (parameter) {
  return request({
    url: api.GetBookList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function UpdatePublicationUser (parameter) {
  return request({
    url: api.UpdatePublicationUser,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function UpdateSuggestedPublicationToClaim (parameter) {
  return request({
    url: api.UpdateSuggestedPublicationToClaim,
    method: 'post',
    data: parameter
  })
}