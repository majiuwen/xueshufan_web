import request from '@/utils/request'
const api = {
  getNewDevelopments: '/userSummary/getNewDevelopments',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getNewDevelopments (parameter) {
  return request({
    url: api.getNewDevelopments,
    method: 'post',
    data: parameter
  })
}