import request from '@/utils/request'

const api = {
  getStatsUserYearlyList: '/statsUserLiterature/getStatsUserLiterature',
  getStatsUserList: '/statsUserLiterature/getStatsUserTotal'
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getStatsUserYearlyList (parameter) {
  return request({
    url: api.getStatsUserYearlyList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getStatsUserList (parameter) {
  return request({
    url: api.getStatsUserList,
    method: 'post',
    data: parameter
  })
}

