import request from '@/utils/request'

const api = {
  getUserEmploymentInfo: '/userExperience/getUserEmploymentInfo',
  getUserEducationInfo: '/userExperience/getUserEducationInfo',
  getUserFundingsInfo: '/userExperience/getUserFundingsInfo',
  getUserDistinctionsInfo: '/userExperience/getUserDistinctionsInfo',
  getUserMembershipInfo: '/userExperience/getUserMembershipInfo',
  getUserPeerReviewsInfo: '/userExperience/getUserPeerReviewsInfo',
  getUserContactInfo: '/userExperience/getUserContactInfo',
  getUserAcademicInfo: '/userExperience/getUserAcademicInfo',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserEmploymentInfo (parameter) {
  return request({
    url: api.getUserEmploymentInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserEducationInfo (parameter) {
  return request({
    url: api.getUserEducationInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserFundingsInfo (parameter) {
  return request({
    url: api.getUserFundingsInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserDistinctionsInfo (parameter) {
  return request({
    url: api.getUserDistinctionsInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserMembershipInfo (parameter) {
  return request({
    url: api.getUserMembershipInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserPeerReviewsInfo (parameter) {
  return request({
    url: api.getUserPeerReviewsInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserContactInfo (parameter) {
  return request({
    url: api.getUserContactInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserAcademicInfo (parameter) {
  return request({
    url: api.getUserAcademicInfo,
    method: 'post',
    data: parameter
  })
}