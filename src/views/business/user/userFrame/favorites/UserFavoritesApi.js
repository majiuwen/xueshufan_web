import request from '@/utils/request'

const api = {
  getCollectList: '/userFavorites/getUserLibraryFolderInfo',
  insertCollect: '/userFavorites/insertUserLibraryFolder',
  updateCollect: '/userFavorites/updateUserLibraryFolder',
  deleteCollect: '/userFavorites/deleteUserLibraryFolder',
  getUserLibraryLabelInfoList: '/userLibraryLabel/getUserLibraryLabelInfo',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getCollectList (parameter) {
  return request({
    url: api.getCollectList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function insertCollect (parameter) {
  return request({
    url: api.insertCollect,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function updateCollect (parameter) {
  return request({
    url: api.updateCollect,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function deleteCollect (parameter) {
  return request({
    url: api.deleteCollect,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserLibraryLabelInfoList (parameter) {
  return request({
    url: api.getUserLibraryLabelInfoList,
    method: 'post',
    data: parameter
  })
}
