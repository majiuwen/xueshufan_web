import request from '@/utils/request'

const api = {
  publicationBeingRead: '/userHome/publicationBeingRead',
  getUserManageAccessList: '/userManageAccess/getUserManageAccessList',
  updatePublicDoc:'/userManageAccess/updatePublicDoc'
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function publicationBeingRead (parameter) {
  return request({
    url: api.publicationBeingRead,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserManageAccessList (parameter) {
  return request({
    url: api.getUserManageAccessList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function updatePublicDoc (parameter) {
  return request({
    url: api.updatePublicDoc,
    method: 'post',
    data: parameter
  })
}