import request from '@/utils/request'

const api = {
  getUserInfo: '/userInfo/getUserInfo',
  updUserInfo: '/userInfo/updUserInfo',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserInfo (parameter) {
  return request({
    url: api.getUserInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function updUserInfo (parameter) {
  return request({
    url: api.updUserInfo,
    method: 'post',
    data: parameter
  })
}