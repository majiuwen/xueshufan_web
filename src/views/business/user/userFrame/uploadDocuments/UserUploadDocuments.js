import request from '@/utils/request'

const api = {
  getDocTypeListInfoByGroupCd: '/userAchievements/getDocTypeListInfoByGroupCd',
  insertPublication: '/userAchievements/insertPublication',
  selJournalList: '/userAchievements/getJournalList',
  selConferenceSerieList: '/userAchievements/getConferenceSerieList',
  selConferenceInstanceList: '/userAchievements/getConferenceInstanceList',
  selConferenceSerie: '/userAchievements/getConferenceSerie',
  selJournal: '/userAchievements/getJournal',
  selConferenceInstance: '/userAchievements/getConferenceInstance',

}

/**
 * 获取文献类型
 * @param parameter
 * @returns {*}
 */
export function getDocTypeListInfoByGroupCd (parameter) {
  return request({
    url: api.getDocTypeListInfoByGroupCd,
    method: 'post',
    data: parameter,
  })
}

/**
 * 新增文献
 * @param parameter
 * @returns {*}
 */
export function insertPublication (parameter) {
  return request({
    url: api.insertPublication,
    method: 'post',
    data: parameter,
  })
}

/**
 * 获取期刊名称自动补全List
 * @param parameter
 * @returns {*}
 */
export function selJournalList (parameter) {
  return request({
    url: api.selJournalList,
    method: 'post',
    data: parameter,
  })
}

/**
 * 获取会议系列名称自动补全List
 * @param parameter
 * @returns {*}
 */
export function selConferenceSerieList (parameter) {
  return request({
    url: api.selConferenceSerieList,
    method: 'post',
    data: parameter,
  })
}

/**
 * 获取会议名称自动补全List
 * @param parameter
 * @returns {*}
 */
export function selConferenceInstanceList (parameter) {
  return request({
    url: api.selConferenceInstanceList,
    method: 'post',
    data: parameter,
  })
}

/**
 * 获取期刊
 * @param parameter
 * @returns {*}
 */
export function selJournal (parameter) {
  return request({
    url: api.selJournal,
    method: 'post',
    data: parameter,
  })
}

/**
 * 获取会议系列
 * @param parameter
 * @returns {*}
 */
export function selConferenceSerie (parameter) {
  return request({
    url: api.selConferenceSerie,
    method: 'post',
    data: parameter,
  })
}

/**
 * 获取会议系列
 * @param parameter
 * @returns {*}
 */
export function selConferenceInstance (parameter) {
  return request({
    url: api.selConferenceInstance,
    method: 'post',
    data: parameter,
  })
}