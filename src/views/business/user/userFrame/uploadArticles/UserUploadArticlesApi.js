import request from '@/utils/request'

const api = {
  FileUpload: '/userAchievements/fileUpload',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function FileUpload (parameter) {
  return request({
    url: api.FileUpload,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}