import request from '@/utils/request'

const api = {
  getUserInfo: '/userSummary/getUserInfo',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserInfo (parameter) {
  return request({
    url: api.getUserInfo,
    method: 'post',
    data: parameter
  })
}
