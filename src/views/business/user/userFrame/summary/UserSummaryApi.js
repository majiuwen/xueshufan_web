import request from '@/utils/request'

const api = {
  getStatsUserTotal: '/userSummary/getStatsUserTotal',
  getStatsUserYearlyList: '/userSummary/getStatsUserYearlyList',
  getUserField: '/userSummary/getUserField',
  getUserInstitution: '/userSummary/getUserInstitution',
  getNewDevelopments:'/userSummary/getNewDevelopments',
  updLikeNums:'/userSummary/updLikeNums',
  getFirstLevelComments:'/userNewsComment/getNewsComments',
  getSecondLevelComments:'/userNewsComment/getSecondComments',
  saveNewsComments:'/userNewsComment/saveNewsComments',
  updCommentLikeNums:'/userNewsComment/updCommentLike',
  delNewsComments:'/userNewsComment/delComments',
  delNewsData:'/userNewDevelopments/delNewsData'
}
/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getNewDevelopments (parameter) {
  return request({
    url: api.getNewDevelopments,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getStatsUserTotal (parameter) {
  return request({
    url: api.getStatsUserTotal,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getStatsUserYearlyList (parameter) {
  return request({
    url: api.getStatsUserYearlyList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserField (parameter) {
  return request({
    url: api.getUserField,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getUserInstitution (parameter) {
  return request({
    url: api.getUserInstitution,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function updLikeNums (parameter) {
  return request({
    url: api.updLikeNums,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getFirstLevelComments (parameter) {
  return request({
    url: api.getFirstLevelComments,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getSecondLevelComments (parameter) {
  return request({
    url: api.getSecondLevelComments,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function saveNewsComments (parameter) {
  return request({
    url: api.saveNewsComments,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function updCommentLikeNums (parameter) {
  return request({
    url: api.updCommentLikeNums,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function delNewsComments (parameter) {
  return request({
    url: api.delNewsComments,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function delNewsData (parameter) {
  return request({
    url: api.delNewsData,
    method: 'post',
    data: parameter
  })
}