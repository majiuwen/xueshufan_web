import request from '@/utils/request'

const api = {
  getSearchResultsInstitutionInfo: '/searchResultsInstitution/getSearchResultsInstitutionList',
}

/**
 * 研究方向一览信息取得
 * @param parameter
 * @returns {*}
 */
export function getSearchResultsInstitutionInfo(parameter) {
  return request({
    url: api.getSearchResultsInstitutionInfo,
    method: 'post',
    data: parameter
  })
}