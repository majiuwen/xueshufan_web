import request from '@/utils/request'

const api = {
  getDirectionDetailInfo: '/searchResultsDirectionDetail/getDirectionDetailInfo',
  getStatsFieldYearlyInfo: '/searchResultsDirectionDetail/getStatsFieldYearlyInfo',
  followingBtnUrl: '/searchResultsDirection/updUserFieldFollowingByFieldId',
  getPublicationTop10List: '/searchResultsDirectionDetail/getPublicationFieldList',
}

/**
 * 研究方向详细信息取得
 * @param parameter
 * @returns {*}
 */
export function getDirectionDetailInfo(parameter) {
  return request({
    url: api.getDirectionDetailInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * 领域年度文献统计信息取得
 * @param parameter
 * @returns {*}
 */
export function getStatsFieldYearlyInfo(parameter) {
  return request({
    url: api.getStatsFieldYearlyInfo,
    method: 'post',
    data: parameter
  })
}

/**
 * 关注/取消关注方法
 * @param parameter
 * @returns {*}
 */
export function followOrCancelFollowAction(parameter) {
  return request({
    url: api.followingBtnUrl,
    method: 'post',
    data: parameter
  })
}

/**
 * 领域TOP10论文
 * @param parameter
 * @returns {AxiosPromise}
 */
export function getPublicationTop10List(parameter) {
  return request({
    url: api.getPublicationTop10List,
    method: 'post',
    data: parameter
  })
}