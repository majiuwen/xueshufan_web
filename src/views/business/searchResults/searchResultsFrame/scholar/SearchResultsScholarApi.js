import request from '@/utils/request'

const api = {
  getSearchResultsScholarList: '/searchResultsScholar/getSearchResultsScholarList',
  followingBtnUrl: '/userHome/addRecommendedClickToFollow',
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function getSearchResultsScholarList (parameter) {
  return request({
    url: api.getSearchResultsScholarList,
    method: 'post',
    data: parameter
  })
}

/**
 * }
 * @param parameter
 * @returns {*}
 */
export function followingBtnUrl (parameter) {
  return request({
    url: api.followingBtnUrl,
    method: 'post',
    data: parameter
  })
}