import request from '@/utils/request'

const api = {
  getSearchResultsDirectionInfo: '/searchResultsDirection/getSearchResultsDirectionList',
}

/**
 * 研究方向一览信息取得
 * @param parameter
 * @returns {*}
 */
export function getSearchResultsDirectionInfo(parameter) {
  return request({
    url: api.getSearchResultsDirectionInfo,
    method: 'post',
    data: parameter
  })
}