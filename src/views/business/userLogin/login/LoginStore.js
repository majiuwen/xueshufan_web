import storage from 'store'
import { login, getInfo, logout,loginCheck } from './LoginApi.js'
import {ACCESS_TOKEN, USER_ID, USER_NAME, USER_AVATAR, USER_INFO} from '@/store/mutation-types'
import { welcome } from '@/utils/util'
import Qs from 'qs'

const LoginStore = {
  state: {
    userId: '',
    token: '',
    name: '',
    welcome: '',
    avatar: '',
    roles: [],
    info: {}
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USERID: (state, userId) => {
      state.userId = userId
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    }
  },

  actions: {
    setInfo({ commit }, response){
      commit('SET_INFO', response.data)
      commit('SET_NAME', response.data.nickname)
      commit('SET_AVATAR', response.data.profilePath)
      storage.set(USER_INFO, response.data,30 * 24 * 60 * 60 * 1000)
      storage.set(USER_NAME, response.data.nickname,30 * 24 * 60 * 60 * 1000)
      storage.set(USER_AVATAR, response.data.profilePath,30 * 24 * 60 * 60 * 1000)
    },
    // 登录
    Login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
        //   const result = response.result
          if(response.ret_code === '0'){
            storage.set(ACCESS_TOKEN, response.access_token, 30 * 24 * 60 * 60 * 1000)
            storage.set(USER_ID, response.data.userId,30 * 24 * 60 * 60 * 1000)
            storage.set(USER_INFO, response.data.userInfo,30 * 24 * 60 * 60 * 1000)
            storage.set(USER_NAME, response.data.userInfo.nickname?response.data.userInfo.nickname:response.data.displayName,30 * 24 * 60 * 60 * 1000)
            storage.set(USER_AVATAR, response.data.userInfo.profilePath?response.data.userInfo.profilePath:response.data.profilePhotoPath,30 * 24 * 60 * 60 * 1000)
            commit('SET_TOKEN', response.access_token)
            commit('SET_USERID', response.data.userId)
            commit('SET_INFO', response.data.userInfo)
            commit('SET_NAME', response.data.userInfo.nickname?response.data.userInfo.nickname:response.data.displayName)
            commit('SET_AVATAR', response.data.userInfo.profilePath?response.data.userInfo.profilePath:response.data.profilePhotoPath)
            resolve(response)
          }else {
            reject(response)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo ({ commit }) {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          const result = response.result

          if (result.role && result.role.permissions.length > 0) {
            const role = result.role
            role.permissions = result.role.permissions
            role.permissions.map(per => {
              if (per.actionEntitySet != null && per.actionEntitySet.length > 0) {
                const action = per.actionEntitySet.map(action => { return action.action })
                per.actionList = action
              }
            })
            role.permissionList = role.permissions.map(permission => { return permission.permissionId })
            commit('SET_ROLES', result.role)
            commit('SET_INFO', result)
          } else {
            reject(new Error('getInfo: roles must be a non-null array !'))
          }

          commit('SET_NAME', { name: result.name, welcome: welcome() })
          commit('SET_AVATAR', result.avatar)

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    Logout ({ commit, state }) {
      return new Promise((resolve) => {
        // logout(state.token).then(() => {
          commit('SET_USERID', '')
          commit('SET_NAME', '')
          commit('SET_AVATAR', '')
          commit('SET_TOKEN', '')
          commit('SET_INFO', '')
          commit('SET_ROLES', [])
          storage.remove(ACCESS_TOKEN)
          storage.remove(USER_ID)
          storage.remove(USER_INFO)
          storage.remove(USER_NAME)
          storage.remove(USER_AVATAR)
          resolve()
        // }).catch(() => {
        //   resolve()
        // }).finally(() => {
        // })
      })
    },

    // 登出
    LoginCheck ({ commit, state }) {
      return new Promise((resolve) => {
        loginCheck(Qs.stringify({token:storage.get(ACCESS_TOKEN)})).then(response => {
          if(response.access_token === storage.get(ACCESS_TOKEN)){
            if(response.refresh_token !== storage.get(ACCESS_TOKEN)){
              storage.set(ACCESS_TOKEN, response.refresh_token, 7 * 24 * 60 * 60 * 1000)
              commit('SET_TOKEN', response.refresh_token)
            }
          }
          resolve()
        }).catch(() => {
          resolve()
        }).finally(() => {
        })
      })
    }

  }
}

export default LoginStore
