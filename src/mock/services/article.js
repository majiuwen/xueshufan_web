import Mock from 'mockjs2'
import { builder, getQueryParameters } from '../util'

const titles = [
  'On a Heuristic Point of View about the Creation and conversion of Light',
  'The case for sequencing the Pacific Oyster genome',
  'The case for sequencing the Pacific Oyster genome',
  'The case for sequencing the Pacific Oyster genome',
  'On a Heuristic Point of View about the Creation and conversion of Light',
  'On a Heuristic Point of View about the Creation and conversion of Light',
]

const avatar = ['https://gw.alipayobjects.com/zos/rmsportal/WdGqmHpayyMjiEhcKoVE.png',
  'https://gw.alipayobjects.com/zos/rmsportal/zOsKZmFRdUtvpqCImOVY.png',
  'https://gw.alipayobjects.com/zos/rmsportal/dURIMkkrRFpPgTuzkwnB.png',
  'https://gw.alipayobjects.com/zos/rmsportal/sfjbOqnsXXJgNCjCzDBL.png',
  'https://gw.alipayobjects.com/zos/rmsportal/siCrBXXhmvTQGWPNLBow.png'
]

const covers = [
  'https://gw.alipayobjects.com/zos/rmsportal/uMfMFlvUuceEyPpotzlq.png',
  'https://gw.alipayobjects.com/zos/rmsportal/iZBVOIhGJiAnhplqjvZW.png',
  'https://gw.alipayobjects.com/zos/rmsportal/iXjVmWVHbCJAyqvDxdtx.png',
  'https://gw.alipayobjects.com/zos/rmsportal/gLaIAoVWTtLbBWZNYEMg.png'
]

const owner = [
  '著作',
  '著作章节',
  '会议论文',
  '数据',
  '期刊论文',
  '专利',
  '学位论文',
  '预印本论文'
]

const otherOwner = [
  [
    {
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },
  ],
  [
    {
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },
  ],
  [
    {
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },{
      'name' :'Albert Einstein',
      'href' :'#',
    },
  ],
  [
    {
      'name' :'Dennis Hedgecock',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
  ],
  [
    {
      'name' :'Dennis Hedgecock',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
  ],
  [
    {
      'name' :'Dennis Hedgecock',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Patrick Gaffney',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
    {
      'name' :'Goulletquer Philippe',
      'href' :'#',
    },
  ],
]

const username = [
  '付小小',
  '吴加好',
  '周星星',
  '林东东',
  '曲丽丽'
]

const userId = [
  '1',
  '2',
  '3',
  '4',
  '5'
]

const identity = [
  '教授',
  '博士生',
  '研究生',
  '副教授',
  '院士'
]

const institutions = [
  '清华大学',
  '北京大学',
  '上海交通大学',
  '研究院',
  '复旦大学'
]

const jobTitle = [
  '人工智能（AI）和数据科学 副教授/助理教授',
  '自然语言处理机器学习 副研究员',
  'Wallenberg NTU presidential postdoctoral fellowship 2021',
  '人工智能（AI）和数据科学 副教授/助理教授',
  '自然语言处理机器学习 副研究员'
]

const institutionName = [
  '清华大学',
  '北京大学',
  'Nanyang Technological University',
  '香港岭南大学',
  '谢菲尔德大学'
]

const location = [
  '北京',
  '北京',
  'Singapore',
  '香港',
  '谢菲尔德'
]

const country = [
  '中国',
  '中国',
  'Singapore',
  '中国',
  '英国'
]

const time = [
  '3小时前',
  '3小时前',
  '1天前',
  '3天前',
  '4天前',
  '5天前'
]

const messages = [
  '你引用过他',
  '他引用过你',
  '你保存过他的论文',
  '你关注过他的论文',
  '领域内高被引论文第一作者'
]

const content = [
  '推荐了下面这个论文',
  '推荐了下面这个论文',
  '推荐了下面这个论文',
  '收藏了下面这个论文',
  '收藏了下面这个论文',
  '收藏了下面这个论文',
]

const source = [
  'International Conference on European Electricity Market',
  'International Conference on European Electricity Market',
  'Ontologies-Based Databases and Information Systems',
  'Ontologies-Based Databases and Information Systems',
  'Journal of the American College of Cardiology',
  'Journal of the American College of Cardiology',
]

const downloadBtnFlg = [
  '1',
  '1',
  '1',
  '2',
  '2',
  '2',
]

const followingBtnFlg = [
  '1',
  '1',
  '1',
  '2',
  '2',
  '2',
]

const isSave = [
  true,
  true,
  true,
  false,
  false,
  false,
]

const isLike = [
  true,
  true,
  true,
  false,
  false,
  false,
]

const isFollow = [
  true,
  true,
  true,
  false,
  false,
  false,
]
const description = [
  'An international community of biologists presents the Pacific oyster Crassostrea gigas as a candidate for genome sequencing. This oyster has global distribution and for the past several years the highest annual production of any freshwater or marine organism (4.2 million metric tons, worth $3.5 billion US). Economic and cultural importance of oysters motivates a great deal of biologic research, which provides a compelling rationale for sequencing an oyster genome. ...',
  'An international community of biologists presents the Pacific oyster Crassostrea gigas as a candidate for genome sequencing. This oyster has global distribution and for the past several years the highest annual production of any freshwater or marine organism (4.2 million metric tons, worth $3.5 billion US). Economic and cultural importance of oysters motivates a great deal of biologic research, which provides a compelling rationale for sequencing an oyster genome. ...',
  'An international community of biologists presents the Pacific oyster Crassostrea gigas as a candidate for genome sequencing. This oyster has global distribution and for the past several years the highest annual production of any freshwater or marine organism (4.2 million metric tons, worth $3.5 billion US). Economic and cultural importance of oysters motivates a great deal of biologic research, which provides a compelling rationale for sequencing an oyster genome. ...',
  'This paper introduced the revolutionary idea that light is composed of both energy and particles: quanta for Einstein, photons for history. This concept that physical systems can behave both as waves (energy) and as particles (matter) would be the seed of one of the two pillars of modern physics: quantum mechanics. Sixteen years later, this theory of the photoelectric effect would take Einstein to the summit of science when, in 1921, he received the Nobel Prize for Physics.',
  'This paper introduced the revolutionary idea that light is composed of both energy and particles: quanta for Einstein, photons for history. This concept that physical systems can behave both as waves (energy) and as particles (matter) would be the seed of one of the two pillars of modern physics: quantum mechanics. Sixteen years later, this theory of the photoelectric effect would take Einstein to the summit of science when, in 1921, he received the Nobel Prize for Physics.', 'This paper introduced the revolutionary idea that light is composed of both energy and particles: quanta for Einstein, photons for history. This concept that physical systems can behave both as waves (energy) and as particles (matter) would be the seed of one of the two pillars of modern physics: quantum mechanics. Sixteen years later, this theory of the photoelectric effect would take Einstein to the summit of science when, in 1921, he received the Nobel Prize for Physics.',
  'This paper introduced the revolutionary idea that light is composed of both energy and particles: quanta for Einstein, photons for history. This concept that physical systems can behave both as waves (energy) and as particles (matter) would be the seed of one of the two pillars of modern physics: quantum mechanics. Sixteen years later, this theory of the photoelectric effect would take Einstein to the summit of science when, in 1921, he received the Nobel Prize for Physics.', 'This paper introduced the revolutionary idea that light is composed of both energy and particles: quanta for Einstein, photons for history. This concept that physical systems can behave both as waves (energy) and as particles (matter) would be the seed of one of the two pillars of modern physics: quantum mechanics. Sixteen years later, this theory of the photoelectric effect would take Einstein to the summit of science when, in 1921, he received the Nobel Prize for Physics.',
]
const href = 'https://ant.design'

const hrefA = [
  'Article',
  'Article',
  'Article',
  'Article',
  'Book'
]

const hrefE = [
  'Article',
  'Article',
  'Article',
  'Article',
  'Book'
]

const hrefB = [
  'ElonMusk',
  'ElonMusk',
  'ElonMusk',
  'ElonMusk',
  'ElonMusk'
]

const hrefC = [
  'Patrick',
  'Patrick',
  'Patrick',
  'Patrick',
  'Patrick',
  'Patrick'
]

const hrefD = [
  'Goul',
  'Goul',
  'Goul',
  'Goul',
  'Goul',
  'Goul'
]

const grid1 = [
  'The case for sequencing the Pacific Oyster genome',
  'The case for sequencing the Pacific Oyster genome',
  'The case for sequencing the Pacific Oyster genome',
  'The case for sequencing the Pacific Oyster genome',
  'On a Heuristic Point of View about the Creation and Conversion of Light'
]

const grid2 = [
  'Jan 2014',
  'Jan 2014',
  'Jan 2014',
  'Jan 2014',
  'Dec 1967'
]

const grid3 = [
  'LREC',
  'LREC',
  'LREC',
  'LREC',
  'LREC'
]

const allflg =
  [
    true,
    true,
    false,
    false,
    true,
    true
  ]

const search1=
  [ 'The case for sequencing the Pacific Oyster',
    'On a Heuristic Point of View about the ',
    'The case for sequencing the Pacific Oyster',
    'On a Heuristic Point of View about the ',
    'The case for sequencing the Pacific Oyster',
    'On a Heuristic Point of View about the',
    'The case for sequencing the Pacific Oyster',
    'On a Heuristic Point of View about the'
  ]

const search2=
  [ 'Biology',
    'Genetics',
    'Chemistry',
    'Genome',
    'Genetics',
    'Genomics',
    'Reference',
    'Active'
  ]

const search3=
  [
    '30',
    '75',
    '56',
    '71',
    '29',
    '75',
    '48',
    '60',
    '90',
    '10',
    '35',
    '80'
  ]

const herfG =
  [
    '收藏',
    '收藏',
    '收藏',
    '收藏',
    '收藏',
    '收藏',
    '收藏',
    '收藏',
    '收藏',
    '收藏'
  ]

const herfH =
  [
    '关注',
    '关注',
    '关注',
    '关注',
    '关注',
    '关注',
    '关注',
    '关注',
    '关注',
    '关注'
  ]

const herfK =
  [
    '推荐',
    '推荐',
    '推荐',
    '推荐',
    '推荐',
    '推荐',
    '推荐',
    '推荐',
    '推荐',
    '推荐'
  ]

const herfL =
  [
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享',
    '分享'
  ]

const herfM =
  [
    '11人推荐',
    '22人推荐',
    '33人推荐',
    '44人推荐',
    '55人推荐',
    '66人推荐'
  ]

const herfN =
  [
    '77人收藏',
    '88人收藏',
    '22人收藏',
    '34人收藏',
    '51人收藏',
    '27人收藏'
  ]

const herfO =
  [
    '被引用1004次',
    '被引用1007次',
    '被引用2004次',
    '被引用1224次',
    '被引用1423次',
    '被引用1094次'
  ]

const intCount =
  [
    121,
    243,
    42,
    35,
    66
  ]

const schoolName =
  [
    'Biology',
    'Materials science',
    'Genetics',
    'Computational biology',
    'Chemistry',
    'Genome',
    'International HapMap Project',
    'Genomics',
    'Reference genome',
    'Cancer genome sequencing'
  ]

const article = (options) => {
  const queryParameters = getQueryParameters(options)
  if (queryParameters && !queryParameters.count) {
    queryParameters.count = 5
  }
  const data = []
  for (let i = 0; i < queryParameters.count; i++) {
    const tmpKey = i + 1
    const num = parseInt(Math.random() * (4 + 1), 10)
    data.push({
      id: tmpKey,
      avatar: avatar[num],
      owner: owner[num],
      isSave: isSave[num],
      isLike: isLike[num],
      isFollow: isFollow[num],
      followingBtnFlg: followingBtnFlg[num],
      downloadBtnFlg: downloadBtnFlg[num],
      time: time[num],
      otherOwner: otherOwner[num],
      source: source[num],
      username: username[num],
      userId: userId[num],
      identity: identity[num],
      institution: institutions[num],
      jobTitle: jobTitle[num],
      institutionName: institutionName[num],
      location: location[num],
      country: country[num],
      messages: messages[num],
      content: content[num],
      star: Mock.mock('@integer(1, 999)'),
      percent: Mock.mock('@integer(1, 999)'),
      like: Mock.mock('@integer(1, 999)'),
      message: Mock.mock('@integer(1, 999)'),
      description: description[num],
      href: href,
      title: titles[ i % 8 ],
      updatedAt: Mock.mock('@datetime'),
      grid1: grid1[num],
      grid2: grid2[num],
      grid3: grid3[num],
      hrefA: hrefA[num],
      hrefB: hrefB[num],
      hrefC: hrefC[num],
      hrefD: hrefD[num],
      search1: search1[num],
      allflg : allflg[num],
      herfG  : herfG[num],
      herfK  : herfK[num],
      herfL  : herfL[num],
      herfH  : herfH[num],
      herfM  : herfM[num],
      herfN  : herfN[num],
      herfO  : herfO[num],
      intCount : intCount[num],
      search2 : search2[num],
      search3 : search3[num],
      schoolName : schoolName[num],
      members: [
        {
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/ZiESqWwCXBRQoaPONSJe.png',
          name: '曲丽丽',
          id: 'member1'
        },
        {
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/tBOxZPlITHqwlGjsJWaF.png',
          name: '王昭君',
          id: 'member2'
        },
        {
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/sBxjgqiuHMGRkIjqlQCd.png',
          name: '董娜娜',
          id: 'member3'
        }
      ],
      activeUser: Math.ceil(Math.random() * 100000) + 100000,
      newUser: Math.ceil(Math.random() * 1000) + 1000,
      cover: parseInt(i / 4, 10) % 2 === 0 ? covers[i % 4] : covers[3 - (i % 4)]
    })
  }
  return builder(data)
}

Mock.mock(/\/list\/article/, 'get', article)
