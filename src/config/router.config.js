// eslint-disable-next-line
import { UserLayout, BasicLayout, BlankLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

const RouteView = {
  name: 'RouteView',
  render: (h) => h('router-view')
}

export const asyncRouterMap = [
  {
    path: '/',
    name: 'home',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/user/home',
    children: [
      {
        path: '/user',
        name: 'user',
        redirect: '/user/home',
        component: RouteView,
        meta: { title: 'menu.user', keepAlive: true,  permission: [ 'user' ] },
        children: [
          {
            path: '/user/home',
            name: 'userHome',
            component: () => import('@/views/business/user/home/UserHome'),
            meta: { title: 'menu.user.home', keepAlive: true, permission: [ 'user' ] }
          },
          // user(用户)
          {
            path: '/user/userPage',
            name: 'userPage',
            component: () => import('@/views/business/user/userFrame/UserFrame'),
            meta: { title: 'menu.user',  keepAlive: true, permission: ['user'] },
            children: [
              // 动态
              {
                path: '/user/userPage/UserDynamic',
                name: 'UserDynamic',
                component: () => import('@/views/business/user/userFrame/dynamic/UserDynamic'),
                meta: { title: 'menu.user.UserDynamic', keepAlive: true, permission: [ 'user' ] }
              },
              // 概况
              {
                path: '/user/userPage/UserSummary',
                name: 'UserSummary',
                component: () => import('@/views/business/user/userFrame/summary/UserSummary'),
                meta: { title: 'menu.user.UserSummary', keepAlive: true, permission: [ 'user' ] }
              },
              // 成果
              {
                path: '/user/userPage/UserAchievements',
                name: 'UserAchievements',
                component: () => import('@/views/business/user/userFrame/achievements/UserAchievements'),
                meta: { title: 'menu.user.UserAchievements', keepAlive: true, permission: [ 'user' ] },
                children: [
                  {
                    path: '/user/userPage/UserClaimArticles',
                    name: 'UserClaimArticles',
                    component: () => import('@/views/business/user/userFrame/claimArticles/UserClaimArticles'),
                    meta: { title: 'menu.user.UserClaimArticles', keepAlive: true, permission: [ 'user' ] }
                  },
                  {
                    path: '/user/userPage/UserManageAccess',
                    name: 'UserManageAccess',
                    component: () => import('@/views/business/user/userFrame/manageAccess/UserManageAccess'),
                    meta: { title: 'menu.user.UserManageAccess', keepAlive: true, permission: [ 'user' ] }
                  },
                  {
                    path: '/user/userPage/UserGenerateReport',
                    name: 'UserGenerateReport',
                    component: () => import('@/views/business/user/userFrame/generateReport/UserGenerateReport'),
                    meta: { title: 'menu.user.UserGenerateReport', keepAlive: true, permission: [ 'user' ] }
                  },
                ]
              },
              // 统计
              {
                path: '/user/userPage/UserStatistics',
                name: 'UserStatistics',
                component: () => import('@/views/business/user/userFrame/statistics/UserStatistics'),
                meta: { title: 'menu.user.UserStatistics', keepAlive: true, permission: [ 'user' ] }
              },
              // 经历
              {
                path: '/user/userPage/UserExperience',
                name: 'UserExperience',
                component: () => import('@/views/business/user/userFrame/experience/UserExperience'),
                meta: { title: 'menu.user.UserExperience', keepAlive: true, permission: [ 'user' ] }
              },
              // 收藏
              {
                path: '/user/userPage/UserFavorites',
                name: 'UserFavorites',
                component: () => import('@/views/business/user/userFrame/favorites/UserFavorites'),
                meta: { title: 'menu.user.UserFavorites', keepAlive: true, permission: [ 'user' ] }
              },
            ]
          },
          // 编辑个人资料
          {
            path: '/user/userPage/EditProfile',
            name: 'EditProfile',
            component: () => import('@/views/business/user/userFrame/editProfile/EditProfile'),
            meta: { title: 'menu.user.EditProfile', keepAlive: true,  permission: [ 'user' ] },
          },
        ]
      },
      // searchResults(检索结果)
      {
        path: '/searchResults',
        name: 'searchResults',
        redirect: '/searchResults/searchResultsPage',
        component: RouteView,
        meta: { title: 'menu.searchResults', keepAlive: true,  permission: [ 'searchResults' ] },
        children: [
          {
            path: '/searchResults/searchResultsPage',
            name: 'searchResultsPage',
            redirect: '/searchResults/searchResultsPage/SearchResultsScholar',
            component: () => import('@/views/business/searchResults/searchResultsFrame/SearchResultsFrame'),
            meta: { title: 'menu.searchResults',  keepAlive: true, permission: ['searchResults'] },
            children: [
              // 学者
              {
                path: '/searchResults/searchResultsPage/SearchResultsScholar',
                name: 'SearchResultsScholar',
                component: () => import('@/views/business/searchResults/searchResultsFrame/scholar/SearchResultsScholar'),
                meta: { title: 'menu.searchResults.SearchResultsScholar', keepAlive: true, permission: [ 'searchResults' ] }
              },
              // 文献
              {
                path: '/searchResults/searchResultsPage/SearchResultsArticles',
                name: 'SearchResultsArticles',
                component: () => import('@/views/business/searchResults/searchResultsFrame/articles/SearchResultsArticles'),
                meta: { title: 'menu.searchResults.SearchResultsArticles', keepAlive: true, permission: [ 'searchResults' ] }
              },
              // 研究方向
              {
                path: '/searchResults/searchResultsPage/SearchResultsDirection',
                name: 'SearchResultsDirection',
                component: () => import('@/views/business/searchResults/searchResultsFrame/direction/SearchResultsDirection'),
                meta: { title: 'menu.searchResults.SearchResultsDirection', keepAlive: true, permission: [ 'searchResults' ] }
              },
              // 研究机构
              {
                path: '/searchResults/searchResultsPage/SearchResultsInstitutions',
                name: 'SearchResultsInstitutions',
                component: () => import('@/views/business/searchResults/searchResultsFrame/institutions/SearchResultsInstitutions'),
                meta: { title: 'menu.searchResults.SearchResultsInstitutions', keepAlive: true, permission: [ 'searchResults' ] }
              },
            ]
          },
          // 研究方向详细
          {
            path: '/searchResults/searchResultsPage/searchResultsDirectionDetail',
            name: 'searchResultsDirectionDetail',
            component: () => import('@/views/business/searchResults/searchResultsFrame/directionDetail/SearchResultsDirectionDetail'),
            meta: { title: 'menu.searchResults.searchResultsDirectionDetail', keepAlive: true, permission: ['searchResults'] },
          },
        ]
      },
      // articles(文献)
      {
        path: '/articles',
        name: 'articles',
        redirect: '/articles/articlesPage',
        component: RouteView,
        meta: { title: 'menu.articles',target:'_blank', keepAlive: true,  permission: [ 'articles' ] },
        children: [
          {
            path: '/articles/articlesPage',
            name: 'articlesPage',
            redirect: '/articles/articlesPage/ArticlesFullText',
            component: () => import('@/views/business/articles/articlesFrame/ArticlesFrame'),
            meta: { title: 'menu.articles',target:'_blank',  keepAlive: true, permission: ['articles'] },
            children: [
              // 全文
              {
                path: '/articles/articlesPage/ArticlesFullText',
                name: 'ArticlesFullText',
                component: () => import('@/views/business/articles/articlesFrame/fullText/ArticlesFullText'),
                meta: { title: 'menu.articles.ArticlesFullText',target:'_blank', keepAlive: true, permission: [ 'articles' ] }
              },
              // 参考文献
              {
                path: '/articles/articlesPage/ArticlesReference',
                name: 'ArticlesReference',
                component: () => import('@/views/business/articles/articlesFrame/reference/ArticlesReference'),
                meta: { title: 'menu.articles.ArticlesReference', keepAlive: true, permission: [ 'articles' ] }
              },
              // 引证文献
              {
                path: '/articles/articlesPage/ArticlesCitation',
                name: 'ArticlesCitation',
                component: () => import('@/views/business/articles/articlesFrame/citation/ArticlesCitation'),
                meta: { title: 'menu.articles.ArticlesCitation', keepAlive: true, permission: [ 'articles' ] }
              },
              // 统计
              {
                path: '/articles/articlesPage/ArticlesStatistics',
                name: 'ArticlesStatistics',
                component: () => import('@/views/business/articles/articlesFrame/statistics/ArticlesStatistics'),
                meta: { title: 'menu.articles.ArticlesStatistics', keepAlive: true, permission: [ 'articles' ] }
              },
              // 讨论
              {
                path: '/articles/articlesPage/ArticlesDiscuss',
                name: 'ArticlesDiscuss',
                component: () => import('@/views/business/articles/articlesFrame/discuss/ArticlesDiscuss'),
                meta: { title: 'menu.articles.ArticlesDiscuss', keepAlive: true, permission: [ 'articles' ] }
              },
            ]
          },
        ]
      },
      // scholar(学者)
      {
        path: '/scholar',
        name: 'scholar',
        redirect: '/scholar/scholarPage',
        component: RouteView,
        meta: { title: 'menu.scholar', keepAlive: true,  permission: [ 'scholar' ] },
        children: [
          {
            path: '/scholar/scholarPage',
            name: 'scholarPage',
            component: () => import('@/views/business/scholar/scholarFrame/ScholarFrame'),
            meta: { title: 'menu.scholar',  keepAlive: true, permission: ['scholar'] },
            children: [
              // 概况
              {
                path: '/scholar/scholarPage/ScholarSummary',
                name: 'ScholarSummary',
                component: () => import('@/views/business/user/userFrame/summary/UserSummary'),
                meta: { title: 'menu.scholar.ScholarSummary', keepAlive: true, permission: [ 'scholar' ] }
              },
              // 学术成果
              {
                path: '/scholar/scholarPage/ScholarAchievements',
                name: 'ScholarAchievements',
                component: () => import('@/views/business/user/userFrame/achievements/UserAchievements'),
                meta: { title: 'menu.scholar.ScholarAchievements', keepAlive: true, permission: [ 'scholar' ] },
              },
              // 统计数字
              {
                path: '/scholar/scholarPage/ScholarStatistics',
                name: 'ScholarStatistics',
                component: () => import('@/views/business/user/userFrame/statistics/UserStatistics'),
                meta: { title: 'menu.scholar.ScholarStatistics', keepAlive: true, permission: [ 'scholar' ] }
              },
              // 经历
              {
                path: '/scholar/scholarPage/ScholarExperience',
                name: 'ScholarExperience',
                component: () => import('@/views/business/user/userFrame/experience/UserExperience'),
                meta: { title: 'menu.scholar.ScholarExperience', keepAlive: true, permission: [ 'scholar' ] }
              },
            ]
          },
        ]
      },
      // institutions(学术机构)
      {
        path: '/institutions',
        name: 'institutions',
        redirect: '/institutions/institutionsPage',
        component: RouteView,
        meta: { title: 'menu.institutions', keepAlive: true,  permission: [ 'institutions' ] },
        children: [
          {
            path: '/institutions/institutionsPage',
            name: 'institutionsPage',
            component: () => import('@/views/business/institutions/institutionsFrame/InstitutionsFrame'),
            meta: { title: 'menu.institutions',  keepAlive: true, permission: ['institutions'] },
            children: [
              // 学术概况
              {
                path: '/institutions/institutionsPage/InstitutionsSummary',
                name: 'InstitutionsSummary',
                component: () => import('@/views/business/institutions/institutionsFrame/summary/InstitutionsSummary'),
                meta: { title: 'menu.institutions.InstitutionsSummary', keepAlive: true, permission: [ 'institutions' ] }
              },
              // 学术成果
              {
                path: '/institutions/institutionsPage/InstitutionsAchievements',
                name: 'InstitutionsAchievements',
                component: () => import('@/views/business/institutions/institutionsFrame/achievements/InstitutionsAchievements'),
                meta: { title: 'menu.institutions.InstitutionsAchievements', keepAlive: true, permission: [ 'institutions' ] }
              },
              // 院系
              {
                path: '/institutions/institutionsPage/InstitutionsDepartment',
                name: 'InstitutionsDepartment',
                component: () => import('@/views/business/institutions/institutionsFrame/department/InstitutionsDepartment'),
                meta: { title: 'menu.institutions.InstitutionsDepartment', keepAlive: true, permission: [ 'institutions' ] }
              },
              // 会员
              {
                path: '/institutions/institutionsPage/InstitutionsMember',
                name: 'InstitutionsMember',
                component: () => import('@/views/business/institutions/institutionsFrame/member/InstitutionsMember'),
                meta: { title: 'menu.institutions.InstitutionsMember', keepAlive: true, permission: [ 'institutions' ] }
              },
              // 会员统计
              {
                path: '/institutions/institutionsPage/InstitutionsMemberStatistics',
                name: 'InstitutionsMemberStatistics',
                component: () => import('@/views/business/institutions/institutionsFrame/memberStatistics/InstitutionsMemberStatistics'),
                meta: { title: 'menu.institutions.InstitutionsMemberStatistics', keepAlive: true, permission: [ 'institutions' ] }
              },
            ]
          },
        ]
      },
      // notification(消息)
      {
        path: '/notification',
        name: 'notification',
        redirect: '/notification/notificationPage',
        component: RouteView,
        meta: { title: 'menu.notification', keepAlive: true,  permission: [ 'notification' ] },
        children: [
          {
            path: '/notification/notificationPage',
            name: 'notificationPage',
            component: () => import('@/views/business/notification/notificationFrame/NotificationFrame'),
            meta: { title: 'menu.notification',  keepAlive: true, permission: ['notification'] },
            children: [
              // 消息更新
              {
                path: '/notification/notificationPage/NotificationUpdates',
                name: 'NotificationUpdates',
                component: () => import('@/views/business/notification/notificationFrame/updates/NotificationUpdates'),
                meta: { title: 'menu.notification.NotificationUpdates', keepAlive: true, permission: [ 'notification' ] }
              },
              // 消息私信
              {
                path: '/notification/notificationPage/NotificationMessages',
                name: 'NotificationMessages',
                component: () => import('@/views/business/notification/notificationFrame/messages/NotificationMessages'),
                meta: { title: 'menu.notification.NotificationMessages', keepAlive: true, permission: [ 'notification' ] }
              },
              // 消息全文请求
              {
                path: '/notification/notificationPage/NotificationRequests',
                name: 'NotificationRequests',
                component: () => import('@/views/business/notification/notificationFrame/requests/NotificationRequests'),
                meta: { title: 'menu.notification.NotificationRequests', keepAlive: true, permission: [ 'notification' ] }
              },
            ]
          },
        ]
      },
      // talentMap(人才地图)
      {
        path: '/talentMap',
        name: 'TalentMap',
        component: () => import('@/views/business/talentMap/TalentMap'),
        meta: { title: 'menu.talentMap', keepAlive: true,  permission: [ 'talentMap' ] },
      },
      // 分享
      {
        path: '/articles/articlesPage/ArticlesShare',
        name: 'ArticlesShare',
        component: () => import('@/views/business/articles/articlesFrame/share/ArticlesShare'),
        meta: { title: 'menu.articles.ArticlesShare', keepAlive: true, permission: [ 'articles' ] }
      },
      // 最新动态详细页
      {
        path: '/newsDetails',
        name: 'NewsDetails',
        component: () => import('@/views/business/newsDetails/NewsDetails'),
        meta: { title: 'menu.newsDetails', keepAlive: true,  permission: [ 'newsDetails' ] },
      },
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  // {
  //   path: '/userLogin',
  //   component: UserLayout,
  //   redirect: '/userLogin/login',
  //   hidden: true,
  //   children: [
  //     {
  //       path: 'login',
  //       name: 'login',
  //       component: () => import('@/views/business/userLogin/login/Login.vue')
  //     },
  //     {
  //       path: 'register',
  //       name: 'register',
  //       component: () => import('@/views/business/userLogin/regist/Regist.vue'),
  //     },
  //     {
  //       path: 'register-result',
  //       name: 'registerResult',
  //       component: () => import('@/views/user/RegisterResult')
  //     },
  //     {
  //       path: 'recover',
  //       name: 'recover',
  //       component: undefined
  //     }
  //   ]
  // },
  //用户驾驶舱
  {
    path: '/manage',
    name: 'manage',
    component: UserLayout,
    redirect: '/manage/login',
    children: [
      {
        path: 'login',
        name: 'manageLogin',
        component: () => import('@/views/business/manage/login/Login')
      },
    ]
  },
  {
    path: '/manage/cockpit',
    name: 'cockpit',
    component: () => import('@/views/business/manage/cockpit/Cockpit'),
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('@/views/business/index/Index.vue'),
    children: [
      {
        path: 'register',
        name: 'register',
        component: () => import('@/views/business/userLogin/regist/Regist.vue')
      },
      {
        path: 'login',
        name: 'login',
        component: () => import('@/views/business/userLogin/login/Login.vue')
      },
      {
        path: 'resetPassword',
        name: 'resetPassword',
        component: () => import('@/views/business/userLogin/resetPassword/ResetPassword.vue')
      },
      {
        path: 'perfectUserInfo',
        name: 'perfectUserInfo',
        component: () => import('@/views/business/userLogin/perfectUserInfo/perfectUserInfo.vue')
      }
    ]
  },
  {
    path: '/agreementForUser',
    name: 'agreement',
    component: () => import('@/views/business/userLogin/agreement/Agreement.vue')
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }

]
